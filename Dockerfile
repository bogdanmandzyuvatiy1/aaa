#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine3.15 AS base
WORKDIR /app

# Issue: https://github.com/dotnet/aspnetcore/issues/4699
EXPOSE 8080
ENV ASPNETCORE_URLS=http://*:8080

# Issue: Globalization Invariant Mode is not supported
# https://github.com/dotnet/SqlClient/issues/220
RUN apk add --no-cache icu-libs
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine3.15 AS build
WORKDIR /
COPY . .
RUN dotnet restore "AuthorizationServer.sln"
RUN dotnet build "AuthorizationServer.sln" -c Release -o /app/build

FROM build AS test
LABEL com.skf.aaa.image=test

FROM build AS publish
RUN dotnet publish "src/AuthorizationServer/AuthorizationServer.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
RUN adduser -Ds /bin/sh serviceuser
USER serviceuser
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "AuthorizationServer.dll"]
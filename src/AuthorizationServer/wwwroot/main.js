﻿const showPasswordIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" focusable="false" height="100%" width="100%"><path d="M10 3c3.902 0 6.853 2.22 8.852 6.663L19 10c-2 4.667-5 7-9 7-3.902 0-6.853-2.22-8.852-6.663L1 10l.148-.337C3.147 5.22 6.098 3 10 3Zm0 2a5 5 0 1 0 0 10 5 5 0 0 0 0-10Zm0 2a3 3 0 1 1 0 6 3 3 0 0 1 0-6Z"></path></svg>';
const hidePasswordIcon = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" focusable="false" height="100%" width="100%"><path d="M17.778 2.222a1 1 0 0 1 0 1.414L3.636 17.778a1 1 0 1 1-1.414-1.414L16.364 2.222a1 1 0 0 1 1.414 0ZM16.69 6.139c.814.956 1.535 2.13 2.162 3.524L19 10c-2 4.667-5 7-9 7-1.258 0-2.418-.23-3.478-.693l1.651-1.651a5 5 0 0 0 6.482-6.482l2.035-2.035ZM10 3c1.258 0 2.418.23 3.478.693l-1.65 1.652a5 5 0 0 0-6.483 6.482L3.31 13.86c-.814-.956-1.535-2.13-2.162-3.524L1 10l.148-.337C3.147 5.22 6.098 3 10 3Zm2.996 6.835A3 3 0 0 1 10 13l-.166-.006 3.162-3.16ZM10 7l.166.005-3.161 3.161A3 3 0 0 1 10 7Z"></path></svg>'

$(document).ready(function () {
    $('.show-password-btn').click(function (e) {
        const $siblingInput = $(this).siblings('input');
        const isPassword = $siblingInput.attr('type') === 'password'
        if (isPassword) {
            $siblingInput.attr('type', 'text');
            $(this).html(hidePasswordIcon);
        }
        else {
            $siblingInput.attr('type', 'password');
            $(this).html(showPasswordIcon);
        }

    })
});
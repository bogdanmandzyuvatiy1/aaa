﻿using System.Security.Claims;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.CQS.Account.Queries.Login;
using AuthorizationServer.Attributes;
using AuthorizationServer.Controllers.Base;
using AuthorizationServer.ViewModels;
using MediatR;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers;

public class AccountController : ApiControllerBase
{
    public AccountController(IMediator mediator)
        : base(mediator)
    {
    }

    [Transaction]
    [HttpGet]
    [AllowAnonymous]
    public IActionResult Login(string returnUrl)
    {
        ViewData["ReturnUrl"] = returnUrl;
        return View();
    }

    [Transaction]
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> Login(LoginViewModel model)
    {
        ViewData["ReturnUrl"] = model.ReturnUrl;

        if (ModelState.IsValid)
        {
            var handlerResult = await Mediator.Send(new LoginQuery(model.Username, model.Password));
            if (handlerResult is ISuccessHandlerResult<LoginQueryResponse> result)
            {
                var claims = result.Response.Claims;
                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                await HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));

                if (Url.IsLocalUrl(model.ReturnUrl))
                {
                    return Redirect(model.ReturnUrl);
                }
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
        ModelState.AddModelError("", Constants.ErrorMessages.LoginFailed);
        return View(model);
    }

    [Transaction]
    public async Task Logout()
    {
        var redirectUrl = Request.Query[Constants.Oidc.PostLogoutRedirectUrlQuery].ToString();
        await HttpContext.SignOutAsync(new AuthenticationProperties()
        {
            RedirectUri = !string.IsNullOrEmpty(redirectUrl)
                ? redirectUrl
                : Url.Action(action: nameof(HomeController.Index), "Home")
        });
    }
}
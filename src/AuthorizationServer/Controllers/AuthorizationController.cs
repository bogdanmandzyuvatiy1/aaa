﻿using System.Security.Claims;
using AuthorizationServer.Attributes;
using AuthorizationServer.Constants;
using AuthorizationServer.Infrastructure.Interfaces;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using OpenIddict.Abstractions;
using OpenIddict.Server.AspNetCore;

namespace AuthorizationServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuthorizationController : ControllerBase
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IOptions<Configuration> _config;

    public AuthorizationController(IOptions<Configuration> config, IUnitOfWork unitOfWork)
    {
        _config = config;
        _unitOfWork = unitOfWork;
    }

    [Transaction]
    [HttpGet(ConfigurationConstants.AuthorizationEndpoint)]
    [HttpPost(ConfigurationConstants.AuthorizationEndpoint)]
    [IgnoreAntiforgeryToken]
    public async Task<IActionResult> Authorize()
    {
        var request = HttpContext.GetOpenIddictServerRequest();
        if (request == null)
        {
            return BadRequest(ErrorMessages.OpenIdOperation);
        }

        var result = await HttpContext.AuthenticateAsync(CookieAuthenticationDefaults.AuthenticationScheme);

        if (!result.Succeeded)
        {
            return Challenge(
                authenticationSchemes: CookieAuthenticationDefaults.AuthenticationScheme,
                properties: new AuthenticationProperties
                {
                    RedirectUri = Request.PathBase + Request.Path + QueryString.Create(
                        Request.HasFormContentType ? Request.Form.ToList() : Request.Query.ToList())
                });
        }

        var claims = new List<Claim>
        {
            new(OpenIddictConstants.Claims.Subject, result.Principal.Identity?.Name)
        };

        var claimsIdentity = new ClaimsIdentity(claims, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
        var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

        claimsPrincipal.SetAuthorizationCodeLifetime(TimeSpan.FromSeconds(_config.Value.AuthorizationCodeLifetimeInSec));
        claimsPrincipal.SetAccessTokenLifetime(TimeSpan.FromSeconds(_config.Value.AccessTokenLifetimeInSec));
        claimsPrincipal.SetRefreshTokenLifetime(TimeSpan.FromSeconds(_config.Value.RefreshTokenLifetimeInSec));
        claimsPrincipal.SetIdentityTokenLifetime(TimeSpan.FromSeconds(_config.Value.IdentityTokenLifetimeInSec));

        var scopes = (await _unitOfWork.ScopeRepository.GetScopesByUserNameAsync(result.Principal.Identity?.Name)).ToList();
        scopes.AddRange(request.GetScopes());
        claimsPrincipal.SetScopes(scopes);

        return SignIn(claimsPrincipal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
    }

    [Transaction]
    [HttpPost(ConfigurationConstants.TokenEndpoint), Produces("application/json")]
    public async Task<IActionResult> Exchange()
    {
        var request = HttpContext.GetOpenIddictServerRequest();
        if (request == null)
        {
            return BadRequest(ErrorMessages.OpenIdOperation);
        }

        ClaimsPrincipal claimsPrincipal;

        if (request.IsAuthorizationCodeGrantType() || request.IsRefreshTokenGrantType())
        {
            claimsPrincipal = (await HttpContext.AuthenticateAsync(OpenIddictServerAspNetCoreDefaults.AuthenticationScheme))?.Principal;
        }
        else
        {
            return BadRequest(ErrorMessages.GrantType);
        }

        return SignIn(claimsPrincipal, OpenIddictServerAspNetCoreDefaults.AuthenticationScheme);
    }
}

﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers.Base;

public class ApiControllerBase : Controller
{
    protected IMediator Mediator { get; }

    public ApiControllerBase(IMediator mediator)
    {
        Mediator = mediator;
    }

    protected IActionResult Send<TResponse>(IHandlerResult<TResponse> handleResult)
        where TResponse : class {
        return handleResult switch {
            ISuccessHandlerResult<object> dhr => Ok(dhr.Response),
            INotFoundHandlerResult<object> nfhr => string.IsNullOrWhiteSpace(nfhr.Message) ? NotFound() : NotFound(nfhr.Message),
            IConflictHandlerResult<object> chr => string.IsNullOrWhiteSpace(chr.Message) ? Conflict() : Conflict(chr.Message),
            ICreatedHandlerResult<object> dhr => StatusCode(StatusCodes.Status201Created, dhr.Response),
            _ => throw new ArgumentOutOfRangeException($"Unknown handler result '{handleResult.GetType()}' encountered")
        };
    }
}
﻿using AuthorizationServer.Application.CQS.Roles.Commands.AddRoleScopes;
using AuthorizationServer.Application.CQS.Roles.Commands.CreateRole;
using AuthorizationServer.Application.CQS.Roles.Commands.DeleteRole;
using AuthorizationServer.Application.CQS.Roles.Commands.UpdateRole;
using AuthorizationServer.Application.CQS.Roles.Queries.GetAllRoles;
using AuthorizationServer.Application.CQS.Roles.Queries.GetRoleById;
using AuthorizationServer.Application.CQS.Roles.Queries.GetScopesByRoleId;
using AuthorizationServer.Attributes;
using AuthorizationServer.Controllers.Base;
using AuthorizationServer.Controllers.Mappers;
using AuthorizationServer.Controllers.Requests;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class RolesController : ApiControllerBase
{
    public RolesController(IMediator mediator)
        : base(mediator)
    {
    }

    [Transaction]
    [HttpPost]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status409Conflict)]
    public async Task<IActionResult> CreateRole([FromBody] CreateRoleCommand request, 
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(request, cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpPost("{roleId}/scopes")]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status409Conflict)]
    public async Task<IActionResult> CreateRoleScopes([FromRoute ]int roleId, [FromBody] AddRoleScopesRequest request,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new AddRoleScopesCommand(roleId, request.ScopeIds), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpGet]
    [ProducesResponseType(typeof(GetAllRolesQueryResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAllRoles(CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetAllRolesQuery(), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpGet("{roleId}")]
    [ProducesResponseType(typeof(GetRoleByIdQueryResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetRoleById([FromRoute] int roleId, CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetRoleByIdQuery(roleId), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpGet("{roleId}/scopes")]
    [ProducesResponseType(typeof(GetScopesByRoleIdQueryResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetScopesByRoleId([FromRoute] int roleId, 
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetScopesByRoleIdQuery(roleId), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpPut ("{roleId}")]
    [ProducesResponseType(typeof(UpdateRoleCommandResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status409Conflict)]
    public async Task<IActionResult> UpdateRole(int roleId, [FromBody] UpdateRoleRequest request,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(request.AsCommand(roleId), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpDelete("{roleId}")]
    [ProducesResponseType(typeof(DeleteRoleCommandResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(CreateRoleCommandResponse), StatusCodes.Status404NotFound)]
    public async Task<IActionResult> DeleteRoleById([FromRoute] int roleId, 
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new DeleteRoleCommand(roleId), cancellationToken);
        return Send(handlerResult);
    }
}
﻿namespace AuthorizationServer.Controllers.Requests;

public record UpdateUserRequest(string FirstName, string? LastName, bool? Status,
    string? EmployeeNumber, string? BadgeNumber, DateTime? HireDate, DateTime? TerminationDate);
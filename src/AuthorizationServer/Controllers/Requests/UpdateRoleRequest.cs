﻿using AuthorizationServer.Domain.Roles.RoleScopes;

namespace AuthorizationServer.Controllers.Requests;

public record UpdateRoleRequest(string roleName, IEnumerable<int> RoleScopeIds);
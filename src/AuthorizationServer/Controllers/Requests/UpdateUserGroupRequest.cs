﻿namespace AuthorizationServer.Controllers.Requests;

public record UpdateUserGroupRequest(string Name, IEnumerable<int>? RoleIds);
﻿namespace AuthorizationServer.Controllers.Requests;

public record AddRoleScopesRequest(List<int> ScopeIds);
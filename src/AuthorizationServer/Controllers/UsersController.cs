﻿using AuthorizationServer.Application.CQS.Users.Commands.CreateUser;
using AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUser;
using AuthorizationServer.Application.CQS.Users.Commands.UpdateUser;
using AuthorizationServer.Application.CQS.Users.Queries.GetAllUsers;
using AuthorizationServer.Application.CQS.Users.Queries.GetScopesByUserId;
using AuthorizationServer.Application.CQS.Users.Queries.GetUserById;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Attributes;
using AuthorizationServer.Controllers.Base;
using AuthorizationServer.Controllers.Mappers;
using AuthorizationServer.Controllers.Requests;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UsersController : ApiControllerBase
{
    public UsersController(IMediator mediator)
        : base(mediator)
    {
    }

    [Transaction]
    [ProducesResponseType(typeof(GetScopesByUserIdQueryResponse), StatusCodes.Status200OK)]
    [HttpGet("{id}/scopes")]
    public async Task<IActionResult> GetScopesByUserId([FromRoute] int id, CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetScopesByUserIdQuery(id), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [ProducesResponseType(typeof(GetAllUsersQueryResponse), StatusCodes.Status200OK)]
    [HttpGet]
    public async Task<IActionResult> GetAllUsers(CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetAllUsersQuery(), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [ProducesResponseType(typeof(GetUserByIdQueryResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(GetUserByIdQueryResponse),StatusCodes.Status404NotFound)]
    [HttpGet("{id}")]
    public async Task<IActionResult> GetUserById([FromRoute] int id, CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetUserByIdQuery(id), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [ProducesResponseType(typeof(CreateUserCommandResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(CreateUserCommandResponse),StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(CreateUserCommandResponse),StatusCodes.Status409Conflict)]
    [HttpPost]
    public async Task<IActionResult> CreateUser([FromBody] CreateUserCommand request,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(request, cancellationToken);
        return Send(handlerResult);
    }

    [ProducesResponseType(typeof(UpdateUserCommandResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(UpdateUserCommandResponse),StatusCodes.Status404NotFound)]
    [Transaction]
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateUser(int id, [FromBody] UpdateUserRequest request,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(request.AsCommand(id), cancellationToken);
        return Send(handlerResult);
    }

    [ProducesResponseType(typeof(PartiallyUpdateUserCommandResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(PartiallyUpdateUserCommandResponse),StatusCodes.Status404NotFound)]
    [Transaction]
    [HttpPatch("{id}")]
    public async Task<IActionResult> PartiallyUpdateUser(int id, [FromBody] JsonPatchDocument<UpdateUserDto> patchDocument,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new PartiallyUpdateUserCommand(id, patchDocument), cancellationToken);
        return Send(handlerResult);
    }
}
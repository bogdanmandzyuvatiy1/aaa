using AuthorizationServer.Application.CQS.ResourceServers.Commands.CreateScopes;
using AuthorizationServer.Application.CQS.ResourceServers.Queries.GetScopesByResourceServerId;
using AuthorizationServer.Attributes;
using AuthorizationServer.Controllers.Base;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ResourceServersController : ApiControllerBase
{
    public ResourceServersController(IMediator mediator)
        : base(mediator)
    {
    }

    [Transaction]
    [HttpPost("scopes")]
    public async Task<IActionResult> CreateScopes([FromBody] CreateScopesCommand request, CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(request, cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [HttpGet("{resourceServerId}/scopes")]
    public async Task<IActionResult> GetScopesByApiId([FromRoute] int resourceServerId, CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetScopesByResourceServerIdQuery(resourceServerId), cancellationToken);
        return Send(handlerResult);
    }
}
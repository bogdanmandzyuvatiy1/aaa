﻿using AuthorizationServer.Application.CQS.UserGroups.Commands.CreateUserGroup;
using AuthorizationServer.Application.CQS.UserGroups.Commands.PartiallyUpdateUserGroup;
using AuthorizationServer.Application.CQS.UserGroups.Commands.UpdateUserGroup;
using AuthorizationServer.Application.CQS.UserGroups.Queries.GetAllUserGroups;
using AuthorizationServer.Application.CQS.UserGroups.Queries.GetUserGroupById;
using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Attributes;
using AuthorizationServer.Controllers.Base;
using AuthorizationServer.Controllers.Mappers;
using AuthorizationServer.Controllers.Requests;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace AuthorizationServer.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserGroupsController: ApiControllerBase
{
    public UserGroupsController(IMediator mediator) : base(mediator)
    {
    }
    
    [Transaction]
    [ProducesResponseType(typeof(CreateUserGroupCommandResponse), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(CreateUserGroupCommandResponse),StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(CreateUserGroupCommandResponse),StatusCodes.Status409Conflict)]
    [HttpPost]
    public async Task<IActionResult> CreateUserGroup([FromBody] CreateUserGroupCommand command,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(command, cancellationToken);
        return Send(handlerResult);
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(GetAllUserGroupsQueryResponse), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAllUserGroups(CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetAllUserGroupsQuery(), cancellationToken);
        return Send(handlerResult);
    }
    
    [Transaction]
    [ProducesResponseType(typeof(GetUserGroupByIdQueryResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(GetUserGroupByIdQueryResponse),StatusCodes.Status404NotFound)]
    [HttpGet("{id}")]
    public async Task<IActionResult> GetUserById([FromRoute] int id, CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new GetUserGroupByIdQuery(id), cancellationToken);
        return Send(handlerResult);
    }

    [Transaction]
    [ProducesResponseType(typeof(UpdateUserGroupCommandResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(UpdateUserGroupCommandResponse),StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(UpdateUserGroupCommandResponse),StatusCodes.Status409Conflict)]
    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateUser(int id, [FromBody] UpdateUserGroupRequest request,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(request.AsCommand(id), cancellationToken);
        return Send(handlerResult);
    }
    
    [Transaction]
    [ProducesResponseType(typeof(PartiallyUpdateUserGroupCommandResponse), StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(PartiallyUpdateUserGroupCommandResponse),StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(PartiallyUpdateUserGroupCommandResponse),StatusCodes.Status409Conflict)]
    [HttpPatch("{id}")]
    public async Task<IActionResult> PartiallyUpdateUser(int id, [FromBody] JsonPatchDocument<UpdateUserGroupDto> patchDocument,
        CancellationToken cancellationToken)
    {
        var handlerResult = await Mediator.Send(new PartiallyUpdateUserGroupCommand(id, patchDocument), cancellationToken);
        return Send(handlerResult);
    }
    
}
﻿using AuthorizationServer.Application.CQS.UserGroups.Commands.UpdateUserGroup;
using AuthorizationServer.Controllers.Requests;

namespace AuthorizationServer.Controllers.Mappers;

public static class UpdateUserGroupMapper
{
    public static UpdateUserGroupCommand AsCommand(this UpdateUserGroupRequest request, int id) =>
        new(id, request.Name, request.RoleIds);
}
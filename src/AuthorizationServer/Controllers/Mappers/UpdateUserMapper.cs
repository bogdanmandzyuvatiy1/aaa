﻿using AuthorizationServer.Application.CQS.Users.Commands.UpdateUser;
using AuthorizationServer.Controllers.Requests;

namespace AuthorizationServer.Controllers.Mappers;

public static class UpdateUserMapper
{
    public static UpdateUserCommand AsCommand(this UpdateUserRequest request, int id) =>
        new(id, request.FirstName, request.LastName, request.Status, request.EmployeeNumber, request.BadgeNumber,
            request.HireDate, request.TerminationDate);
}
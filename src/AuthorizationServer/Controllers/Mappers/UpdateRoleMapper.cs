﻿using AuthorizationServer.Application.CQS.Roles.Commands.UpdateRole;
using AuthorizationServer.Controllers.Requests;

namespace AuthorizationServer.Controllers.Mappers;

public static class UpdateRoleMapper
{ public static UpdateRoleCommand AsCommand(this UpdateRoleRequest request, int roleId) =>
        new(roleId, request.roleName, request.RoleScopeIds);
}
﻿using AuthorizationServer.Application;
using AuthorizationServer.Application.CQS.Roles.Rules;
using AuthorizationServer.Application.CQS.UserGroups.Rules;
using AuthorizationServer.Application.CQS.Users.Rules;
using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;
using AuthorizationServer.Domain.Users.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Interfaces;
using AuthorizationServer.Infrastructure.SeedWork;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddCore(this IServiceCollection services)
    {
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<IUserUniquenessChecker, UsernameUniquenessChecker>();
        services.AddScoped<IRoleNameUniquenessChecker, RoleNameUniquenessChecker>();
        services.AddScoped<IUserGroupUniquenessChecker, UserGroupUniquenessChecker>();
        services.AddAutoMapper(typeof(ApplicationStartup).Assembly);
        services.AddMediatR(typeof(ApplicationStartup).Assembly);
        return services;
    }

    public static IServiceCollection AddDatabase<TDb>(this IServiceCollection services, string connectionString,
        bool useInMemoryDatabase)
        where TDb : DbContext
    {
        services.AddDbContext<TDb>(
            options =>
            {
                if (useInMemoryDatabase)
                {
                    options.UseInMemoryDatabase(nameof(AuthContext));
                    options.UseOpenIddict();
                }
                else
                {
                    options.UseSqlServer(connectionString).EnableSensitiveDataLogging();
                    services.AddHealthChecks().AddDbContextCheck<AuthContext>();
                    options.UseOpenIddict();
                }
            });
        return services;
    }
}
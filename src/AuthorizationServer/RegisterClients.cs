﻿using AuthorizationServer.Infrastructure.Database;
using Microsoft.Extensions.Options;
using OpenIddict.Abstractions;

namespace AuthorizationServer;

public class RegisterClients : IHostedService
{
    private readonly IServiceProvider _serviceProvider;
    private IOpenIddictApplicationManager _manager;
    private readonly IOptions<Configuration> _config;

    public RegisterClients(IServiceProvider serviceProvider, IOptions<Configuration> config)
    {
        _serviceProvider = serviceProvider;
        _config = config;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        using var scope = _serviceProvider.CreateScope();

        var context = scope.ServiceProvider.GetRequiredService<AuthContext>();
        await context.Database.EnsureCreatedAsync(cancellationToken);

        _manager = scope.ServiceProvider.GetRequiredService<IOpenIddictApplicationManager>();
        await RegisterClient(Constants.Oidc.TestClientIdentifier,
            new List<Uri> {new(_config.Value.ClientUrl), new(_config.Value.PostmanUrl)},
            Constants.Oidc.TestClientDisplayName,
            cancellationToken);
    }

    public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    private async Task RegisterClient(string clientId, IEnumerable<Uri> redirectUris, string displayName,
        CancellationToken cancellationToken)
    {
        if (await _manager.FindByClientIdAsync(clientId, cancellationToken) is null)
        {
            var appDescriptor = new OpenIddictApplicationDescriptor
            {
                ClientId = clientId,
                DisplayName = displayName,
                Permissions =
                {
                    OpenIddictConstants.Permissions.Endpoints.Authorization,
                    OpenIddictConstants.Permissions.Endpoints.Token,

                    OpenIddictConstants.Permissions.GrantTypes.AuthorizationCode,
                    OpenIddictConstants.Permissions.GrantTypes.RefreshToken,

                    OpenIddictConstants.Permissions.ResponseTypes.Code
                },
                Requirements =
                {
                    OpenIddictConstants.Requirements.Features.ProofKeyForCodeExchange
                }
            };
            foreach (var redirectUri in redirectUris)
            {
                appDescriptor.RedirectUris.Add(redirectUri);
            }

            await _manager.CreateAsync(appDescriptor, cancellationToken);
        }
    }
}
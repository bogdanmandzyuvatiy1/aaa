﻿using AuthorizationServer.Application.CQS.ResourceServers.Commands.CreateScopes;
using MediatR;
using SKF.MES.Laboratory.Module.AAA;

namespace AuthorizationServer
{
    public class RegisterScopes : IHostedService
    {
        private readonly IMediator _mediator;
        public RegisterScopes(IMediator mediator)
        {
            _mediator = mediator;
        }
        public async Task StartAsync(CancellationToken cancellationToken = default)
        {
            await _mediator.Send(new CreateScopesCommand(Enum.GetNames(typeof(Scopes)).ToList(), ResourceServerName.Name), cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
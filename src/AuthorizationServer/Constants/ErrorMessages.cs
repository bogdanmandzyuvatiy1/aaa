﻿namespace AuthorizationServer.Constants;

public static class ErrorMessages
{
    public const string LoginFailed = "Incorrect login or password.";
    public const string UserAlreadyExists = "User with such username already exists.";
    public const string OpenIdOperation = "The OpenID Connect request cannot be retrieved.";
    public const string GrantType = "The specified grant type is not supported.";
}
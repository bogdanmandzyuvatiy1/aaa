﻿namespace AuthorizationServer.Constants;

public static class ConfigurationConstants
{
    public const string LoginPath = "/account/login";
    public const string AuthorizationEndpoint = "/authorize";
    public const string TokenEndpoint = "/token";
    public const string HealthProbe = "/healthz";
    public const string ReadyProbe = "/ready";
    public const string ConnectionName = "AuthServer";
    public const string ClientUrlName = "ClientUrl";
}
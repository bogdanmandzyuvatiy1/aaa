﻿using SKF.MES.Laboratory.Module.AAA;

namespace AuthorizationServer.Constants;

public static class Oidc
{
    public const string TestClientIdentifier = "task-system-client";
    public const string ClientIdentifier = ClientName.ClientIdentifier;
    public const string TestClientDisplayName = "TaskSystemClient";
    public const string PostLogoutRedirectUrlQuery = "post_logout_redirect_uri";
}
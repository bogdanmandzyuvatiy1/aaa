﻿using System.Reflection;
using System.Security.Cryptography.X509Certificates;

namespace AuthorizationServer;

public class SigningIssuerCertificate
{
    public X509Certificate2 TokenEncryptionCertificate()
    {
        var bytes = ReadResourceAsBytes("encryption-certificate");
        return new X509Certificate2(bytes);
    }

    public X509Certificate2 TokenSigningCertificate()
    {
        var bytes = ReadResourceAsBytes("signing-certificate");
        return new X509Certificate2(bytes);
    }

    private byte[] ReadResourceAsBytes(string fileName)
    {
        var resource = Assembly.GetAssembly(typeof(SigningIssuerCertificate))
            ?.GetManifestResourceStream($"AuthorizationServer.Certificates.{fileName}.pfx");
        var ms = new MemoryStream();
        resource?.CopyTo(ms);
        return ms.ToArray();
    }
}
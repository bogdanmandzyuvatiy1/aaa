﻿namespace AuthorizationServer;

public class Configuration
{
    // Section names
    public const string Tokens = "Tokens";
    public const string Urls = "Urls";
    // Urls section values
    public string ClientUrl { get; set; }
    public string PostmanUrl { get; set; }
    // Tokens section values
    public double AccessTokenLifetimeInSec { get; set; }
    public double RefreshTokenLifetimeInSec { get; set; }
    public double IdentityTokenLifetimeInSec { get; set; }
    public double AuthorizationCodeLifetimeInSec { get; set; }
}
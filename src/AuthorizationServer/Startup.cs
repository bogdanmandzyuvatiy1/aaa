﻿using AuthorizationServer.Extensions;
using AuthorizationServer.Infrastructure.Database;
using Microsoft.AspNetCore.Authentication.Cookies;
using AuthorizationServer.Constants;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace AuthorizationServer;

public class Startup
{
    public Startup(IWebHostEnvironment environment, IConfiguration configuration)
    {
        Environment = environment;
        Configuration = configuration;
    }

    private IWebHostEnvironment Environment { get; }
    private IConfiguration Configuration { get; }

    public bool UseInMemoryDatabase => string.Equals(Environment.EnvironmentName, EnvironmentNames.Testing, StringComparison.OrdinalIgnoreCase);

    public void ConfigureServices(IServiceCollection services)
    {
        var issuerSigningCertificate = new SigningIssuerCertificate();

        services.AddOptions<Configuration>().Bind(Configuration.GetSection(AuthorizationServer.Configuration.Tokens))
            .ValidateDataAnnotations();
        services.AddOptions<Configuration>().Bind(Configuration.GetSection(AuthorizationServer.Configuration.Urls))
            .ValidateDataAnnotations();

        services.AddMvc();

        services.AddControllersWithViews().AddNewtonsoftJson();
        
        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                options.LoginPath = ConfigurationConstants.LoginPath;
            });
        services.AddHealthChecks().AddDbContextCheck<AuthContext>();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        
        var connectionString =
            Configuration.GetConnectionString(ConfigurationConstants.ConnectionName) ??
            $"Server=tcp:{Configuration["DbConnectionString:Server"]},{Configuration["DbConnectionString:Port"] ?? "1433"};" +
            $"Initial Catalog={Configuration["DbConnectionString:Database"]};" +
            $"Persist Security Info=False;" +
            $"User ID={Configuration["DbConnectionString:Username"]};" +
            $"Password={Configuration["DbConnectionString:Password"]};" +
            $"MultipleActiveResultSets=False;" +
            $"Encrypt=True;" +
            $"TrustServerCertificate=True;" +
            $"Connection Timeout=30;";
        
        services.AddDatabase<AuthContext>(
            connectionString, UseInMemoryDatabase);

        services.AddCore();
        services.AddOpenIddict()
            .AddCore(options =>
            {
                options
                    .UseEntityFrameworkCore()
                    .UseDbContext<AuthContext>();
            })

            .AddServer(options =>
            {
                options
                    .AllowAuthorizationCodeFlow()
                    .RequireProofKeyForCodeExchange()
                    .AllowRefreshTokenFlow();
                options
                    .SetAuthorizationEndpointUris(ConfigurationConstants.AuthorizationEndpoint)
                    .SetTokenEndpointUris(ConfigurationConstants.TokenEndpoint);
                options
                    .AddEncryptionCertificate(issuerSigningCertificate.TokenEncryptionCertificate())
                    .AddSigningCertificate(issuerSigningCertificate.TokenSigningCertificate());
                options
                    .UseAspNetCore()
                    .EnableTokenEndpointPassthrough()
                    .EnableAuthorizationEndpointPassthrough();
            });
        services.AddHostedService<RegisterClients>();

        services.AddCors(options =>
            options.AddDefaultPolicy(policyBuilder =>
            {
                policyBuilder.WithOrigins(Configuration.GetSection(AuthorizationServer.Configuration.Urls)[ConfigurationConstants.ClientUrlName])
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowCredentials();
            }));
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseCors();
        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapDefaultControllerRoute();
            endpoints.MapHealthChecks(ConfigurationConstants.ReadyProbe, new HealthCheckOptions { Predicate = _ => false });
            endpoints.MapHealthChecks(ConfigurationConstants.HealthProbe, new HealthCheckOptions
            {
                ResultStatusCodes =
                {
                    [HealthStatus.Healthy] = StatusCodes.Status200OK,
                    [HealthStatus.Degraded] = StatusCodes.Status200OK,
                    [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
                }
            });
        });
    }
}
﻿using System.Transactions;
using Microsoft.AspNetCore.Mvc.Filters;

namespace AuthorizationServer.Attributes;

public class TransactionAttribute : Attribute, IAsyncActionFilter
{ 
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        using var transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);
        var actionExecutedContext = await next();
        if (actionExecutedContext.Exception == null)
            transactionScope.Complete();
        else
        {
            transactionScope.Dispose();
            throw actionExecutedContext.Exception;
        }
    }
}
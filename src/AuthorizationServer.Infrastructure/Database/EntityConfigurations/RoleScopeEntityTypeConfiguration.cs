﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations;

public class RoleScopeTypeConfiguration: IEntityTypeConfiguration<RoleScopeEntity>
{
    public void Configure(EntityTypeBuilder<RoleScopeEntity> builder)
    {
        builder.ToTable(Constants.TableNames.RoleScopes);

        builder.HasKey(us => us.Id);
        builder.HasAlternateKey(us => new {us.ScopeId, us.RoleId});
        
        builder
            .HasOne(us => us.RoleEntity)
            .WithMany(u => u.RoleScopes)
            .HasForeignKey(us => us.RoleId);
        
        builder
            .HasOne(us => us.ScopeEntity)
            .WithMany(u => u.RoleScopes)
            .HasForeignKey(us => us.ScopeId);
    }
}
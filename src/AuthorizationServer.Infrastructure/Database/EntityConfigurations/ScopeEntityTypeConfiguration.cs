﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations;

public class ScopeEntityTypeConfiguration: IEntityTypeConfiguration<ScopeEntity>
{
    public void Configure(EntityTypeBuilder<ScopeEntity> builder)
    {
        builder.ToTable(Constants.TableNames.Scopes);
        
        builder.HasKey(s => s.Id);

        builder
            .HasOne(s => s.ResourceServerEntity)
            .WithMany(a => a.Scopes)
            .HasForeignKey(us => us.ResourceServerId);

        builder.Property(s => s.ScopeName).IsRequired().HasMaxLength(Constants.TablePropertiesLength.ScopeNameMaxLength);
    }
}
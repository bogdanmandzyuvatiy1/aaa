﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations
{
    public class UserGroupEntityTypeConfiguration: IEntityTypeConfiguration<UserGroupEntity>
    {
        public void Configure(EntityTypeBuilder<UserGroupEntity> builder)
        {
            builder.ToTable(Constants.TableNames.UserGroups);

            builder.HasKey(r => r.Id);

            builder.Property(r => r.UserGroupName).IsRequired().HasMaxLength(Constants.TablePropertiesLength.UserGroupNameMaxLength);
            builder.HasIndex(r => r.UserGroupName).IsUnique();
        }
    }
}

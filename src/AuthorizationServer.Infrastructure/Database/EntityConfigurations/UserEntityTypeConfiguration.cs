﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations;

public class UserEntityTypeConfiguration: IEntityTypeConfiguration<UserEntity>
{
    public void Configure(EntityTypeBuilder<UserEntity> builder)
    {
        builder.ToTable(Constants.TableNames.Users);
        
        builder.HasKey(u => u.Id);

        builder
            .HasOne(u => u.UserGroupEntity)
            .WithMany(a => a.Users)
            .HasForeignKey(u => u.UserGroupId);

        builder.Property(u => u.Username).IsRequired().HasMaxLength(Constants.TablePropertiesLength.UserUsernameMaxLength);
        builder.Property(u => u.Password).IsRequired().HasMaxLength(Constants.TablePropertiesLength.UserPasswordMaxLength);
        builder.Property(u => u.FirstName).IsRequired().HasMaxLength(Constants.TablePropertiesLength.UserFirstNameMaxLength);
    }
}
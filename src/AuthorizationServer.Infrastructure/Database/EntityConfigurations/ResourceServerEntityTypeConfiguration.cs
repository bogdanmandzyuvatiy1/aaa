﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations;

public class ResourceServerEntityTypeConfiguration : IEntityTypeConfiguration<ResourceServerEntity>
{
    public void Configure(EntityTypeBuilder<ResourceServerEntity> builder)
    {
        builder.ToTable(Constants.TableNames.ResourceServers);
        
        builder.HasKey(r => r.Id);

        builder.Property(r => r.ResourceServerName).IsRequired().HasMaxLength(Constants.TablePropertiesLength.ResourceServerNameMaxLength);
        builder.HasIndex(r => r.ResourceServerName).IsUnique();
    }
}
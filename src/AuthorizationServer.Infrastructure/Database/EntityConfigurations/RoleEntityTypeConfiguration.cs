﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations;

public class RoleEntityTypeConfiguration : IEntityTypeConfiguration<RoleEntity>
{
    public void Configure(EntityTypeBuilder<RoleEntity> builder)
    {
        builder.ToTable(Constants.TableNames.Roles);

        builder.HasKey(r => r.Id);
        
        builder.Property(r => r.RoleName).IsRequired().HasMaxLength(Constants.TablePropertiesLength.RoleNameMaxLength);
    }
}
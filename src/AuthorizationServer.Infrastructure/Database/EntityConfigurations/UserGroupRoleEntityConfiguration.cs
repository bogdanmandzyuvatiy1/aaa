﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AuthorizationServer.Infrastructure.Database.EntityConfigurations;

public class UserGroupRoleEntityConfiguration: IEntityTypeConfiguration<UserGroupRoleEntity>
{
    public void Configure(EntityTypeBuilder<UserGroupRoleEntity> builder)
    {
        builder.ToTable(Constants.TableNames.UserGroupRoles);
        
        builder.HasKey(us => us.Id);
        builder.HasAlternateKey(us => new {us.RoleId, us.UserGroupId});
        
        builder
            .HasOne(us => us.RoleEntity)
            .WithMany(u => u.UserGroupRoles)
            .HasForeignKey(us => us.RoleId);
        
        builder
            .HasOne(us => us.UserGroupEntity)
            .WithMany(u => u.UserGroupRoles)
            .HasForeignKey(us => us.UserGroupId);
    }
}
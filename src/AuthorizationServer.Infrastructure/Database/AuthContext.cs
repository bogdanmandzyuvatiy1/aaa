﻿using AuthorizationServer.Infrastructure.Constants;
using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Database;

public class AuthContext : DbContext
{
    public DbSet<UserEntity> Users { get; set; }
    public DbSet<ResourceServerEntity> ResourceServers { get; set; }
    public DbSet<ScopeEntity> Scopes { get; set; }
    public DbSet<RoleScopeEntity> RoleScopes { get; set; }
    public DbSet<RoleEntity> Roles { get; set; }
    public DbSet<UserGroupEntity> UserGroups { get; set; }
    public DbSet<UserGroupRoleEntity> UserGroupRoles { get; set; }
    private readonly bool _addSeedData;

    public AuthContext()
    {
    }
    public AuthContext(DbContextOptions<AuthContext> options)
        : base(options)
    {
        var environment = Environment.GetEnvironmentVariable(EnvironmentNames.EnvironmentVariableName);
        _addSeedData = environment == EnvironmentNames.Testing;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(AuthContext).Assembly);
        if (_addSeedData)
        {
            modelBuilder.SeedTestData();
        }
    }
}
﻿using AuthorizationServer.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Database;

public static class ModelBuilderExtensions
{
    public static void SeedTestData(this ModelBuilder modelBuilder)
    {
        var seedUserGroup = new UserGroupEntity
        {
            Id = 1,
            UserGroupName = "Management"
        };
        var seedUserGroup1 = new UserGroupEntity
        {
            Id = 2,
            UserGroupName = "Workers"
        };
        modelBuilder.Entity<UserGroupEntity>().HasData(seedUserGroup, seedUserGroup1);

        var seedRole = new RoleEntity
        {
            Id = 1,
            RoleName = "User"
        };
        var seedRole1 = new RoleEntity
        {
            Id = 2,
            RoleName = "Admin"
        };
        modelBuilder.Entity<RoleEntity>().HasData(seedRole, seedRole1);

        var seedUser = new UserEntity
        {
            Id = 1,
            UserGroupId = seedRole1.Id,
            FirstName = "Name",
            Password = "password",
            Username = "dummy"
        };
        modelBuilder.Entity<UserEntity>().HasData(seedUser);

        var seedResourceServer = new ResourceServerEntity
        {
            Id = 1,
            ResourceServerName = "TaskSystem"
        };
        modelBuilder.Entity<ResourceServerEntity>().HasData(seedResourceServer);

        ScopeEntity seedScope = new()
        {
            Id = 1,
            ScopeName = "UPDATE_ANY_TASK",
            ResourceServerId = seedResourceServer.Id
        },
            seedScope1 = new()
            {
                Id = 2,
                ScopeName = "DELETE_ANY_TASK",
                ResourceServerId = seedResourceServer.Id
            },
            seedScope2 = new()
            {
                Id = 3,
                ScopeName = "ASSIGN_TASK_TO_ANY_USER",
                ResourceServerId = seedResourceServer.Id
            };
        modelBuilder.Entity<ScopeEntity>().HasData(seedScope, seedScope1, seedScope2);

        RoleScopeEntity seedRoleScopes1 = new()
        {
            Id = 1,
            RoleId = seedRole1.Id,
            ScopeId = seedScope.Id,
        },
            seedRoleScopes2 = new()
            {
                Id = 2,
                RoleId = seedRole1.Id,
                ScopeId = seedScope1.Id,
            };
        modelBuilder.Entity<RoleScopeEntity>().HasData(seedRoleScopes1, seedRoleScopes2);
    }
}


﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Repositories;

public class RoleRepository : Repository<RoleEntity>, IRoleRepository
{
    private readonly AuthContext _context;

    public RoleRepository(AuthContext context)
        : base(context)
    {
        _context = context;
    }

    public async Task<RoleEntity?> GetWithDetailsAsync(int roleId, CancellationToken cancellationToken)
    {
        return await _context.Roles.Include(s => s.RoleScopes)
            .ThenInclude(rs => rs.ScopeEntity)
            .SingleOrDefaultAsync(r => r.Id == roleId, cancellationToken);
    }

    public async Task<IEnumerable<RoleEntity>> GetAllWithDetailsAsync(CancellationToken cancellationToken)
    {
        return await _context.Roles.Include(s => s.RoleScopes)
            .ThenInclude(rs => rs.ScopeEntity)
            .ToListAsync(cancellationToken);
    }
    
    public bool Exists(string roleName)
    {
        return _context.Roles.Any(r => r.RoleName == roleName);
    }

    public bool Exists(int id)
    {
        return _context.Roles.Any(r => r.Id == id);
    }
    
    public bool ExistsWithAnotherId(string roleName, int id)
    {
        return _context.Roles.Any(r => r.RoleName == roleName && r.Id != id);
    }
}
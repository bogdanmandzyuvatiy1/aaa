﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Repositories;

public class UserGroupRepository : Repository<UserGroupEntity>, IUserGroupRepository
{
    private readonly AuthContext _context;

    public UserGroupRepository(AuthContext context) : base(context)
    {
        _context = context;
    }

    public bool Exists(string name)
    {
        return _context.UserGroups.Any(u => u.UserGroupName == name);
    }

    public bool Exists(int? id)
    {
        return _context.UserGroups.Any(u => u.Id == id);
    }

    public bool ExistsWithAnotherId(string name, int id)
    {
        return _context.UserGroups.Any(u => u.UserGroupName == name && u.Id != id);
    }

    public async Task<UserGroupEntity?> GetWithDetailsAsync(int id, CancellationToken cancellationToken)
    {
        return await _context.UserGroups.Include(ug => ug.UserGroupRoles)
            .ThenInclude(ugr => ugr.RoleEntity)
            .FirstOrDefaultAsync(ug => ug.Id == id, cancellationToken);
    }

    public async Task<List<UserGroupEntity>> GetAllWithDetailsAsync(CancellationToken cancellationToken)
    {
        return await _context.UserGroups.Include(ug => ug.UserGroupRoles).ThenInclude(ugr => ugr.RoleEntity)
            .ToListAsync(cancellationToken);
    }
}
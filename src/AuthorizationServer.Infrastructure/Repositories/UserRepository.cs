﻿using System.Linq.Expressions;
using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Repositories;

public class UserRepository : Repository<UserEntity>, IUserRepository
{
    private readonly AuthContext _context;

    public UserRepository(AuthContext context)
        : base(context)
    {
        _context = context;
    }

    public bool Exists(string username)
    {
        return _context.Users.Any(u => u.Username == username);
    }

    public bool ExistsWithAnotherId(string username, int id)
    {
        return _context.Users.Any(u => u.Username == username && u.Id != id);
    }

    public async Task<IEnumerable<UserEntity>> GetAsync(
        Expression<Func<UserEntity, bool>>? filter = null)
    {
        IQueryable<UserEntity> query = _context.Users;
        
        if (filter != null)
        {
            query = query.Where(filter);
        }

        return await query.ToListAsync();
    }

    public async Task<IEnumerable<UserEntity>> GetAllAsync()
    {
        return await _context.Users.OrderBy(u => u.Username).ToListAsync();
    }
}
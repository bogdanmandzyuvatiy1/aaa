﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Repositories;

public class ResourceServerRepository: Repository<ResourceServerEntity>, IResourceServerRepository
{
    private readonly AuthContext _context;

    public ResourceServerRepository(AuthContext context)
        : base(context)
    {
        _context = context;
    }
    public async Task<ResourceServerEntity?> GetAsync(string resourceServerName)
    {
        return await _context.ResourceServers.Where(r => r.ResourceServerName == resourceServerName)
            .Include(r => r.Scopes).SingleOrDefaultAsync();
    }
}
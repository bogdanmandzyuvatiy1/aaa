﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace AuthorizationServer.Infrastructure.Repositories;

public class ScopeRepository : Repository<ScopeEntity>, IScopeRepository
{
    private readonly AuthContext _context;

    public ScopeRepository(AuthContext context)
        : base(context)
    {
        _context = context;
    }

    public bool Exists(int scopeId)
    {
        return _context.Scopes.Any(u => u.Id == scopeId);
    }

    public async Task<IEnumerable<string>> GetScopesByUserNameAsync(string userName)
    {
        return await _context.Users.Include(u => u.UserGroupEntity)
            .ThenInclude(ug => ug.UserGroupRoles)
            .ThenInclude(ugr => ugr.RoleEntity)
            .ThenInclude(r => r.RoleScopes)
            .ThenInclude(rs => rs.ScopeEntity)
            .Where(u => u.Username == userName)
            .SelectMany(u => u.UserGroupEntity.UserGroupRoles)
            .SelectMany(ugr => ugr.RoleEntity.RoleScopes)
            .Select(rs => rs.ScopeEntity.ScopeName)
            .ToListAsync();
    }

    public async Task<IEnumerable<ScopeEntity>> GetScopesByResourceServerIdAsync(int resourceServerId)
    {
        return await _context.Scopes.Where(s => s.ResourceServerId == resourceServerId).ToListAsync();
    }

    public async Task<IEnumerable<ScopeEntity>> GetScopesByRoleIdAsync(int roleId)
    {
        return await _context.Roles.Include(r => r.RoleScopes)
            .ThenInclude(rs => rs.ScopeEntity)
            .Where(r => r.Id == roleId)
            .SelectMany(r => r.RoleScopes)
            .Select(rs => rs.ScopeEntity)
            .ToListAsync();
    }

    public async Task<IEnumerable<ScopeEntity>> GetScopesByUserIdAsync(int userId)
    {
        return await _context.Users.Include(u => u.UserGroupEntity)
            .ThenInclude(ug => ug.UserGroupRoles)
            .ThenInclude(ugr => ugr.RoleEntity)
            .ThenInclude(r => r.RoleScopes)
            .ThenInclude(rs => rs.ScopeEntity)
            .Where(u => u.Id == userId)
            .SelectMany(u => u.UserGroupEntity.UserGroupRoles)
            .SelectMany(ugr => ugr.RoleEntity.RoleScopes)
            .Select(rs => rs.ScopeEntity)
            .ToListAsync();
    }
}
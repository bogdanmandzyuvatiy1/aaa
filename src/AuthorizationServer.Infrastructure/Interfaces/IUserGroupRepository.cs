﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Infrastructure.Interfaces;

public interface IUserGroupRepository: IRepository<UserGroupEntity>
{
    public bool Exists(string name);
    public bool ExistsWithAnotherId(string name, int id);
    bool Exists(int? id);
    public Task<UserGroupEntity?> GetWithDetailsAsync(int id, CancellationToken cancellationToken);
    public Task<List<UserGroupEntity>> GetAllWithDetailsAsync(CancellationToken cancellationToken);
}
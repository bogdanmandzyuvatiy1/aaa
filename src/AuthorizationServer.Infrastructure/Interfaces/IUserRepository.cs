﻿using System.Linq.Expressions;
using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Infrastructure.Interfaces;

public interface IUserRepository : IRepository<UserEntity>
{
    Task<IEnumerable<UserEntity>> GetAsync(Expression<Func<UserEntity, bool>> filter);
    public bool Exists(string username);
    public bool ExistsWithAnotherId(string username, int id);
}
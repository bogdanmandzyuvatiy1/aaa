﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Infrastructure.Interfaces;

public interface IScopeRepository : IRepository<ScopeEntity>
{
    bool Exists(int scopeId);
    Task<IEnumerable<ScopeEntity>> GetScopesByResourceServerIdAsync(int resourceServerId);
    Task<IEnumerable<ScopeEntity>> GetScopesByRoleIdAsync(int roleId);
    Task<IEnumerable<ScopeEntity>> GetScopesByUserIdAsync(int userId);
    Task<IEnumerable<string>> GetScopesByUserNameAsync(string userName);
    
}
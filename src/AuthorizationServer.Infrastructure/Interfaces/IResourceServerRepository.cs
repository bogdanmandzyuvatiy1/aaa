﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Infrastructure.Interfaces;

public interface IResourceServerRepository : IRepository<ResourceServerEntity>
{
    Task<ResourceServerEntity?> GetAsync(string resourceServerName);
}
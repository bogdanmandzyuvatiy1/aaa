﻿using Art3mis.Common.Core.Infrastructure;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Infrastructure.Interfaces;

public interface IRoleRepository : IRepository<RoleEntity>
{
    Task<RoleEntity?> GetWithDetailsAsync(int roleId, CancellationToken cancellationToken);
    Task<IEnumerable<RoleEntity>> GetAllWithDetailsAsync(CancellationToken cancellationToken);
    bool Exists(string roleName);
    public bool Exists(int id);
    bool ExistsWithAnotherId(string roleName, int id);
}
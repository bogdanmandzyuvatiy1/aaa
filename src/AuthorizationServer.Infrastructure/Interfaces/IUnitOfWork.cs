﻿namespace AuthorizationServer.Infrastructure.Interfaces;

public interface IUnitOfWork
{
    IScopeRepository ScopeRepository { get; }
    IUserRepository UserRepository  { get; }
    IResourceServerRepository ResourceServerRepository { get; }
    IUserGroupRepository UserGroupRepository { get; }
    IRoleRepository RoleRepository { get; }
    Task<int> SaveAsync(CancellationToken cancellationToken = default);
}
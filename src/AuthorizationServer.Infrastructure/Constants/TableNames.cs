﻿namespace AuthorizationServer.Infrastructure.Constants;

public static class TableNames
{
    public const string ResourceServers = "ResourceServers";
    public const string UserGroups = "UserGroups";
    public const string Scopes = "Scopes";
    public const string Users = "Users";
    public const string Roles = "Roles";
    public const string RoleScopes = "RoleScopes";
    public const string UserGroupRoles = "UserGroupRoles";
}
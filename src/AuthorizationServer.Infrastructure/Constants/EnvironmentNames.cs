﻿namespace AuthorizationServer.Infrastructure.Constants;

public static class EnvironmentNames
{
    public static readonly string Testing = "Testing";
    public static readonly string EnvironmentVariableName = "ASPNETCORE_ENVIRONMENT";
}
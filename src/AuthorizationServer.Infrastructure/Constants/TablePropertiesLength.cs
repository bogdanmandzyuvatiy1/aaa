﻿namespace AuthorizationServer.Infrastructure.Constants;

public static class TablePropertiesLength
{
    public const int UserScopeNameLength = 200;
    public const int UserUsernameMaxLength = 100;
    public const int UserPasswordMaxLength = 250;
    public const int UserFirstNameMaxLength = 250;
    public const int ScopeNameMaxLength = 100;
    public const int ResourceServerNameMaxLength = 100;
    public const int UserGroupNameMaxLength = 100;
    public const int RoleNameMaxLength = 100;
}
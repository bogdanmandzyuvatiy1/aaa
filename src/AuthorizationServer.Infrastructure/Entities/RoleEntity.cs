﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class RoleEntity : IBaseEntity
{
    public int Id { get; set; }
    public string RoleName { get; set; }
    public ICollection<RoleScopeEntity> RoleScopes { get; set; }
    public ICollection<UserGroupRoleEntity> UserGroupRoles { get; set; }
}
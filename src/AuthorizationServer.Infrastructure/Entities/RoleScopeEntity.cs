﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class RoleScopeEntity : IBaseEntity
{
    public int Id { get; set; }
    public int RoleId { get; set; }
    public int ScopeId { get; set; }
    public RoleEntity RoleEntity { get; set; }
    public ScopeEntity ScopeEntity { get; set; }
}
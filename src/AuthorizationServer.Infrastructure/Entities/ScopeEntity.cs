﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class ScopeEntity : IBaseEntity
{
    public int Id { get; set; }
    public int ResourceServerId { get; set; }
    public string ScopeName { get; set; }
    public ResourceServerEntity ResourceServerEntity { get; set; }
    public ICollection<RoleScopeEntity> RoleScopes { get; set; }
}
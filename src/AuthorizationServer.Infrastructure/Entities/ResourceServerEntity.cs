﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class ResourceServerEntity: IBaseEntity
{
    public int Id { get; set; }
    public string ResourceServerName { get; set; }
    public ICollection<ScopeEntity> Scopes { get; set; }
}
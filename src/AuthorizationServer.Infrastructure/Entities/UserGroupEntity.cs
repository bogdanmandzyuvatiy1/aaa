﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class UserGroupEntity: IBaseEntity
{
    public int Id { get; set; }
    public string UserGroupName { get; set; }
    public ICollection<UserEntity> Users { get; set; }
    public ICollection<UserGroupRoleEntity> UserGroupRoles { get; set; }
}
﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class UserEntity : IBaseEntity
{
    public int Id { get; set; }
    public string Username { get; set; }
    public int? UserGroupId { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string? LastName { get; set; }
    public bool? Status { get; set; }
    public string? EmployeeNumber { get; set; }
    public string? BadgeNumber { get; set; }
    public DateTime? HireDate { get; set; }
    public DateTime? TerminationDate { get; set; }
    public UserGroupEntity UserGroupEntity { get; set; }
}
﻿using Art3mis.Common.Core.Data;

namespace AuthorizationServer.Infrastructure.Entities;

public class UserGroupRoleEntity: IBaseEntity
{
    public int Id { get; set; }
    public int UserGroupId { get; set; }
    public int RoleId { get; set; }
    public RoleEntity RoleEntity { get; set; }
    public UserGroupEntity UserGroupEntity { get; set; }
}
﻿using AuthorizationServer.Infrastructure.Database;
using AuthorizationServer.Infrastructure.Interfaces;
using AuthorizationServer.Infrastructure.Repositories;

namespace AuthorizationServer.Infrastructure.SeedWork;

public sealed class UnitOfWork : IUnitOfWork, IDisposable
{
    private readonly AuthContext _authContext;
    private IUserRepository? _userRepository;
    private IScopeRepository? _scopeRepository;
    private IResourceServerRepository? _resourceServerRepository;
    private IUserGroupRepository? _userGroupRepository;
    private IRoleRepository? _roleRepository;

    public IResourceServerRepository ResourceServerRepository
    {
        get
        {
            _resourceServerRepository ??= new ResourceServerRepository(_authContext);
            return _resourceServerRepository;
        }
    }

    public IUserGroupRepository UserGroupRepository
    {
        get
        {
            _userGroupRepository ??= new UserGroupRepository(_authContext);
            return _userGroupRepository;
        }
    }
        public IRoleRepository RoleRepository
    {
        get
        {
            _roleRepository ??= new RoleRepository(_authContext);
            return _roleRepository;
        }
    }
    

    public IUserRepository UserRepository
    {
        get
        {
            _userRepository ??= new UserRepository(_authContext);
            return _userRepository;
        }
    }
    
    public IScopeRepository ScopeRepository
    {
        get
        {
            _scopeRepository ??= new ScopeRepository(_authContext);
            return _scopeRepository;
        }
    }
    
    public UnitOfWork(AuthContext authContext)
    {
        _authContext = authContext;
    }
    
    public async Task<int> SaveAsync(CancellationToken cancellationToken = default(CancellationToken))
    {
        return await _authContext.SaveChangesAsync(cancellationToken);
    }

    private bool _disposed;

    private void Dispose(bool disposing)
    {
        if (!_disposed && disposing)
        {
            _authContext.Dispose();
        }
        _disposed = true;
    }
    
    public void Dispose()
    {
        Dispose(true);
    }
}
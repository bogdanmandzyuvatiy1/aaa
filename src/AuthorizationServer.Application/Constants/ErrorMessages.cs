﻿namespace AuthorizationServer.Application.Constants;

public static class ErrorMessages
{
    public const string LoginFailedError = "Incorrect login or password.";
    public const string ResourceServerWithThisIdNotFound = "Resource server with such an id not found"; 
    public const string UserWithSuchIdNotFound = $"User with such an id not found";
    public const string RoleWithSuchIdNotFound = $"Role with such an id not found";
    public const string UserGroupWithSuchIdNotFound = $"UserGroup with such an id not found";
    public const string RolesWithSuchIdsNotFound = @$"Roles with such ids are not exist: ";
    public const string ScopesWithSuchIdsNotFound = "Scopes with such ids are not exist: ";
}
﻿namespace AuthorizationServer.Application.DTOs.Scopes;

public class ScopeDto
{
    public int Id { get; set; }
    public string ScopeName { get; set; }
    public int ResourceServerId { get; set; }
}
﻿namespace AuthorizationServer.Application.DTOs.Users;

public record UpdateUserByAdminDto(int? UserGroupId, string FirstName, string? LastName, bool? Status,
    string? EmployeeNumber, string? BadgeNumber, DateTime? HireDate, DateTime? TerminationDate);
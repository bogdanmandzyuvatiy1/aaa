﻿namespace AuthorizationServer.Application.DTOs.Users;

public record UserDto(int Id, string Username, int UserGroupId, string FirstName, string LastName, bool Status,
    string EmployeeNumber, string BadgeNumber, DateTime HireDate, DateTime TerminationDate);
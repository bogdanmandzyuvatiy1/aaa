﻿namespace AuthorizationServer.Application.DTOs.Users;

public record UpdateUserDto(string FirstName, string LastName, bool Status, string EmployeeNumber,
    string BadgeNumber, DateTime HireDate, DateTime TerminationDate);
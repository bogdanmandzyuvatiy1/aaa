﻿namespace AuthorizationServer.Application.DTOs.UserGroups;

public record UserGroupWithRolesDto
{
    public int Id { get; init; }
    public string UserGroupName { get; init; }
    public ICollection<UserGroupRoleDto> RoleDtos { get; init; }
}
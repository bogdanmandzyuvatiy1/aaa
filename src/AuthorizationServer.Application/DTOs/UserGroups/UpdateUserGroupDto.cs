﻿namespace AuthorizationServer.Application.DTOs.UserGroups;

public record UpdateUserGroupDto
{
    public string UserGroupName{ get; init; }
    public IEnumerable<int>? RoleIds{ get; init; }
}
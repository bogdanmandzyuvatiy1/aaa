﻿namespace AuthorizationServer.Application.DTOs.UserGroups;

public record UserGroupRoleDto
{
    public int Id { get; init; }
    public string RoleName { get; init; }
}
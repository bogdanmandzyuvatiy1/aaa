﻿namespace AuthorizationServer.Application.DTOs.UserGroups;

public record UserGroupDto
{
    public int Id { get; set; }
    public string UserGroupName { get; set; }
    public IEnumerable<int>? RoleIds { get; set; }
}
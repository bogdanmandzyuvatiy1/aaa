﻿namespace AuthorizationServer.Application.DTOs.Roles;

public record RoleDto
{
    public int Id { get; init; }
    public string RoleName { get; init; }
    public ICollection<int>? ScopeIds { get; init; }
}
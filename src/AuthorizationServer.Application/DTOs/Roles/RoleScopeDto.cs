﻿namespace AuthorizationServer.Application.DTOs.Roles;

public record RoleScopeDto
{
    public int Id { get; init; }
    public string ScopeName { get; init; }
}
﻿namespace AuthorizationServer.Application.DTOs.Roles;

public record RoleWithScopesDto
{
    public int Id { get; init; }
    public string RoleName { get; init; }
    public ICollection<RoleScopeDto> ScopeDtos { get; init; }
}
﻿using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Infrastructure.Entities;
using AutoMapper;

namespace AuthorizationServer.Application.MappingProfiles.Users;

public class UserMappingProfile : Profile
{
    public UserMappingProfile()
    {
        CreateMap<User, UserEntity>()
            .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password.Password))
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
            .ForMember(dest => dest.UserGroupId, opt => opt.MapFrom(src => src.UserGroupId))
            .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
            .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
            .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => src.EmployeeNumber))
            .ForMember(dest => dest.BadgeNumber, opt => opt.MapFrom(src => src.BadgeNumber))
            .ForMember(dest => dest.HireDate, opt => opt.MapFrom(src => src.HireDate))
            .ForMember(dest => dest.TerminationDate, opt => opt.MapFrom(src => src.TerminationDate))
            .ReverseMap();
        
        CreateMap<UserEntity, UpdateUserDto>();

        CreateMap<UserEntity, UpdateUserByAdminDto>();

        CreateMap<UserEntity, UserDto>();
    }
}
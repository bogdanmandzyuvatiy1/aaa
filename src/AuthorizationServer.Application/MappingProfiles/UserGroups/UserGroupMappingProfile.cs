﻿using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Domain.UserGroups;
using AuthorizationServer.Domain.UserGroups.Roles;
using AuthorizationServer.Infrastructure.Entities;
using AutoMapper;

namespace AuthorizationServer.Application.MappingProfiles.UserGroups;

public class UserGroupMappingProfile : Profile
{
    public UserGroupMappingProfile()
    {
        CreateMap<UserGroup, UserGroupEntity>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.UserGroupName, opt => opt.MapFrom(src => src.UserGroupName))
            .ForMember(dest => dest.UserGroupRoles, opt => opt.MapFrom(src => src.UserGroupRoles))
            .ReverseMap();
        CreateMap<UserGroupRole, UserGroupRoleEntity>()
            .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.RoleId))
            .ReverseMap();

        CreateMap<UserGroupEntity, UserGroupWithRolesDto>()
            .ForMember(dest => dest.RoleDtos, opt => opt.MapFrom(src => src.UserGroupRoles.Select(ugr => ugr.RoleEntity)));
        CreateMap<RoleEntity, UserGroupRoleDto>();

        CreateMap<UserGroupEntity, UpdateUserGroupDto>()
            .ForMember(dest => dest.RoleIds, opt => opt.MapFrom(src => src.UserGroupRoles.Select(ugr => ugr.RoleId)));

        CreateMap<UserGroupEntity, UserGroupDto>()
            .ForMember(dest => dest.RoleIds, opt => opt.MapFrom(src => src.UserGroupRoles.Select(ugr => ugr.RoleId)));
    }
}
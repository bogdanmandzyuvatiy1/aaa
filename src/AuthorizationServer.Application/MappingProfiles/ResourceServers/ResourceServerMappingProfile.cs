﻿using AuthorizationServer.Domain.ResourceServers;
using AuthorizationServer.Domain.ResourceServers.Scopes;
using AuthorizationServer.Infrastructure.Entities;
using AutoMapper;

namespace AuthorizationServer.Application.MappingProfiles.ResourceServers;

public class ResourceServerMappingProfile : Profile
{
    public ResourceServerMappingProfile()
    {
        CreateMap<ResourceServerEntity, ResourceServer>().ReverseMap();
        CreateMap<ResourceServerScope, ScopeEntity>().ReverseMap();
    }
}
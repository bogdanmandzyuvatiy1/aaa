﻿using AuthorizationServer.Application.DTOs.Scopes;
using AuthorizationServer.Infrastructure.Entities;
using AutoMapper;

namespace AuthorizationServer.Application.MappingProfiles.Scopes;

public class ScopeMappingProfile : Profile
{
    public ScopeMappingProfile()
    {
        CreateMap<ScopeEntity, ScopeDto>();
    }
}
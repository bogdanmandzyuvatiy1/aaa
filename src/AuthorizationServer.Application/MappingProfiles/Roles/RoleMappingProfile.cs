﻿using AuthorizationServer.Application.DTOs.Roles;
using AuthorizationServer.Domain.Roles;
using AuthorizationServer.Domain.Roles.RoleScopes;
using AuthorizationServer.Infrastructure.Entities;
using AutoMapper;

namespace AuthorizationServer.Application.MappingProfiles.Roles;

public class RoleMappingProfile : Profile
{
    public RoleMappingProfile()
    {
        CreateMap<RoleEntity, Role>()
            .ReverseMap();
        CreateMap<RoleScopeEntity, RoleScope>()
            .ReverseMap();
        CreateMap<RoleEntity, RoleDto>()
            .ForMember(dest => dest.ScopeIds, opt => opt.MapFrom(src => src.RoleScopes.Select(ugr => ugr.ScopeId))).ReverseMap();
        CreateMap<RoleEntity, RoleWithScopesDto>()
            .ForMember(dest => dest.ScopeDtos, opt => opt.MapFrom(src => src.RoleScopes.Select(ugr => ugr.ScopeEntity)));
        CreateMap<ScopeEntity, RoleScopeDto>();
    }
}
﻿using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Interfaces;

namespace AuthorizationServer.Application.CQS.UserGroups.Rules;

public class UserGroupUniquenessChecker : IUserGroupUniquenessChecker
{
    private readonly IUnitOfWork _unitOfWork;

    public UserGroupUniquenessChecker(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    public bool IsUnique(string username, int id = 0)
    {
        if (id == 0)
        {
            return !_unitOfWork.UserGroupRepository.Exists(username);
        }

        return !_unitOfWork.UserGroupRepository.ExistsWithAnotherId(username, id);
    }
}
﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.UpdateUserGroup;

public record UpdateUserGroupCommand
    (int Id, string Name, IEnumerable<int>? RoleIds) : IRequest<IHandlerResult<UpdateUserGroupCommandResponse>>;
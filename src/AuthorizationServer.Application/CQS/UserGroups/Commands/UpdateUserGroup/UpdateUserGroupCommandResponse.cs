﻿using AuthorizationServer.Application.DTOs.UserGroups;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.UpdateUserGroup;

public class UpdateUserGroupCommandResponse
{
    public UpdateUserGroupCommandResponse(UserGroupDto userGroupDto)
    {
        UserGroupDto = userGroupDto;
    }

    public UserGroupDto UserGroupDto { get; set; }
};
﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Domain.UserGroups;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.UpdateUserGroup;

public class UpdateUserGroupCommandHandler : RequestHandlerBase<UpdateUserGroupCommand, UpdateUserGroupCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IUserGroupUniquenessChecker _userGroupUniquenessChecker;

    public UpdateUserGroupCommandHandler(IUnitOfWork unitOfWork, IMapper mapper,
        IUserGroupUniquenessChecker userGroupUniquenessChecker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _userGroupUniquenessChecker = userGroupUniquenessChecker;
    }

    public override async Task<IHandlerResult<UpdateUserGroupCommandResponse>> Handle(UpdateUserGroupCommand command,
        CancellationToken cancellationToken)
    {
        var userGroupEntity = await _unitOfWork.UserGroupRepository.GetWithDetailsAsync(command.Id, cancellationToken);
        if (userGroupEntity == null)
        {
            return NotFound<UpdateUserGroupCommandResponse>(ErrorMessages.UserGroupWithSuchIdNotFound);
        }

        if (command.RoleIds != null)
        {
            var nonExistingRolesIds = command.RoleIds.Where(id => !_unitOfWork.RoleRepository.Exists(id)).ToList();
            if (nonExistingRolesIds.Any())
            {
                return NotFound<UpdateUserGroupCommandResponse>(ErrorMessages.RolesWithSuchIdsNotFound +
                                                                $"[{string.Join("], [", nonExistingRolesIds)}]");
            }
        }

        var userGroup = _mapper.Map<UserGroup>(userGroupEntity);
        var result = userGroup.Update(command.Name, _userGroupUniquenessChecker, command.RoleIds);

        if (!result.IsSuccess)
        {
            return Conflict<UpdateUserGroupCommandResponse>(result.Message);
        }

        _mapper.Map(result.Resource, userGroupEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new UpdateUserGroupCommandResponse(_mapper.Map<UserGroupDto>(userGroupEntity)));
    }
}
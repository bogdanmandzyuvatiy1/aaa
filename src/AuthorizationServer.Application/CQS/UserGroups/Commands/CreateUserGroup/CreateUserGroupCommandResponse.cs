﻿using AuthorizationServer.Application.DTOs.UserGroups;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.CreateUserGroup;

public class CreateUserGroupCommandResponse
{
    public CreateUserGroupCommandResponse(UserGroupDto userGroupDto)
    {
        UserGroupDto = userGroupDto;
    }

    public UserGroupDto UserGroupDto { get; set; }
}
﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.CreateUserGroup;

public record CreateUserGroupCommand
    (string Name, IEnumerable<int>? RoleIds) : IRequest<IHandlerResult<CreateUserGroupCommandResponse>>;
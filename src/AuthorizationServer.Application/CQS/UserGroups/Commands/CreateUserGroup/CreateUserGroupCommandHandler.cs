﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Domain.UserGroups;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.CreateUserGroup;

public class CreateUserGroupCommandHandler : RequestHandlerBase<CreateUserGroupCommand, CreateUserGroupCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IUserGroupUniquenessChecker _userGroupUniquenessChecker;

    public CreateUserGroupCommandHandler(IUnitOfWork unitOfWork, IMapper mapper,
        IUserGroupUniquenessChecker userGroupUniquenessChecker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _userGroupUniquenessChecker = userGroupUniquenessChecker;
    }

    public override async Task<IHandlerResult<CreateUserGroupCommandResponse>> Handle(CreateUserGroupCommand command,
        CancellationToken cancellationToken)
    {
        if (command.RoleIds != null)
        {
            var nonExistingRolesIds = command.RoleIds.Where(id => !_unitOfWork.RoleRepository.Exists(id)).ToList();
            if (nonExistingRolesIds.Any())
            {
                return NotFound<CreateUserGroupCommandResponse>(ErrorMessages.RolesWithSuchIdsNotFound + $"[{ string.Join("], [", nonExistingRolesIds)}]");
            } 
        }

        var result = UserGroup.Create(command.Name, _userGroupUniquenessChecker, command.RoleIds);
        if (!result.IsSuccess)
        {
            return Conflict<CreateUserGroupCommandResponse>(result.Message);
        }

        var userGroupEntity = await _unitOfWork.UserGroupRepository.AddAsync(_mapper.Map<UserGroupEntity>(result.Resource));
        await _unitOfWork.SaveAsync(cancellationToken);
        return Created(new CreateUserGroupCommandResponse (_mapper.Map<UserGroupDto>(userGroupEntity)));
    }
}
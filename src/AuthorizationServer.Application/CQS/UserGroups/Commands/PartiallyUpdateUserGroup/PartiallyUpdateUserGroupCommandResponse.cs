﻿using AuthorizationServer.Application.DTOs.UserGroups;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.PartiallyUpdateUserGroup;

public class PartiallyUpdateUserGroupCommandResponse
{
    public PartiallyUpdateUserGroupCommandResponse(UserGroupDto userGroupDto)
    {
        UserGroupDto = userGroupDto;
    }

    public UserGroupDto UserGroupDto { get; set; }
};
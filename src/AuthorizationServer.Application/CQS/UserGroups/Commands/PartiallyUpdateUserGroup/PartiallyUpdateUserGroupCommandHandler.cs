﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Domain.UserGroups;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.PartiallyUpdateUserGroup;

public class PartiallyUpdateUserGroupCommandHandler : RequestHandlerBase<PartiallyUpdateUserGroupCommand,
    PartiallyUpdateUserGroupCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IUserGroupUniquenessChecker _userGroupUniquenessChecker;

    public PartiallyUpdateUserGroupCommandHandler(IUnitOfWork unitOfWork, IMapper mapper,
        IUserGroupUniquenessChecker userGroupUniquenessChecker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _userGroupUniquenessChecker = userGroupUniquenessChecker;
    }

    public override async Task<IHandlerResult<PartiallyUpdateUserGroupCommandResponse>> Handle(
        PartiallyUpdateUserGroupCommand command,
        CancellationToken cancellationToken)
    {
        var userGroupEntity = await _unitOfWork.UserGroupRepository.GetWithDetailsAsync(command.Id, cancellationToken);
        if (userGroupEntity == null)
        {
            return NotFound<PartiallyUpdateUserGroupCommandResponse>(ErrorMessages.UserGroupWithSuchIdNotFound);
        }

        var userGroupDto = _mapper.Map<UpdateUserGroupDto>(userGroupEntity);
        command.PatchDoc.ApplyTo(userGroupDto);

        if (userGroupDto.RoleIds != null)
        {
            var nonExistingRolesIds = userGroupDto.RoleIds.Where(id => !_unitOfWork.RoleRepository.Exists(id)).ToList();
            if (nonExistingRolesIds.Any())
            {
                return NotFound<PartiallyUpdateUserGroupCommandResponse>(ErrorMessages.RolesWithSuchIdsNotFound +
                                                                         $"[{string.Join("], [", nonExistingRolesIds)}]");
            }
        }

        var userGroup = _mapper.Map<UserGroup>(userGroupEntity);
        var result = userGroup.Update(userGroupDto.UserGroupName, _userGroupUniquenessChecker, userGroupDto.RoleIds);

        if (!result.IsSuccess)
        {
            return Conflict<PartiallyUpdateUserGroupCommandResponse>(result.Message);
        }

        _mapper.Map(result.Resource, userGroupEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new PartiallyUpdateUserGroupCommandResponse(_mapper.Map<UserGroupDto>(userGroupEntity)));
    }
}
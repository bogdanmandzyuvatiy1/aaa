﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.UserGroups;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;

namespace AuthorizationServer.Application.CQS.UserGroups.Commands.PartiallyUpdateUserGroup;

public record PartiallyUpdateUserGroupCommand
    (int Id, JsonPatchDocument<UpdateUserGroupDto> PatchDoc) : IRequest<IHandlerResult<PartiallyUpdateUserGroupCommandResponse>>;
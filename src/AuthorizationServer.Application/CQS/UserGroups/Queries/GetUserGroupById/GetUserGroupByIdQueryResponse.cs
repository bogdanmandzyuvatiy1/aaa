﻿using AuthorizationServer.Application.DTOs.UserGroups;

namespace AuthorizationServer.Application.CQS.UserGroups.Queries.GetUserGroupById;

public class GetUserGroupByIdQueryResponse
{
    public GetUserGroupByIdQueryResponse(UserGroupWithRolesDto userGroupWithRolesDto)
    {
        UserGroupWithRolesDto = userGroupWithRolesDto;
    }

    public UserGroupWithRolesDto UserGroupWithRolesDto  { get; set; }
}
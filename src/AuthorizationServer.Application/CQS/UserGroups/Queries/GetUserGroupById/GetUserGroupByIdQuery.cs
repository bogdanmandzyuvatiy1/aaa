﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.UserGroups.Queries.GetUserGroupById;

public record GetUserGroupByIdQuery(int Id): IRequest<IHandlerResult<GetUserGroupByIdQueryResponse>>;
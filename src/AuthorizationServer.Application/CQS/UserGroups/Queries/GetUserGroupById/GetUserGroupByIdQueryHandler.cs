﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.UserGroups.Queries.GetUserGroupById;

public class GetUserGroupByIdQueryHandler : RequestHandlerBase<GetUserGroupByIdQuery, GetUserGroupByIdQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetUserGroupByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<GetUserGroupByIdQueryResponse>> Handle(GetUserGroupByIdQuery query,
        CancellationToken cancellationToken)
    {
        var userGroupEntity = await _unitOfWork.UserGroupRepository.GetWithDetailsAsync(query.Id, cancellationToken);
        return userGroupEntity == null
            ? NotFound<GetUserGroupByIdQueryResponse>(ErrorMessages.UserGroupWithSuchIdNotFound)
            : Data(new GetUserGroupByIdQueryResponse(_mapper.Map<UserGroupWithRolesDto>(userGroupEntity)));
    }
}
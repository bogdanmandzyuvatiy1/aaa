﻿using AuthorizationServer.Application.DTOs.UserGroups;

namespace AuthorizationServer.Application.CQS.UserGroups.Queries.GetAllUserGroups;

public class GetAllUserGroupsQueryResponse
{
    public GetAllUserGroupsQueryResponse(List<UserGroupWithRolesDto> userGroupDtos)
    {
        UserGroupDtos = userGroupDtos;
    }

    public List<UserGroupWithRolesDto> UserGroupDtos { get; set; }
}
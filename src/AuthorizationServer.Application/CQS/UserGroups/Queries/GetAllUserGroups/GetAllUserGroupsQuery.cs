﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.UserGroups.Queries.GetAllUserGroups;

public record GetAllUserGroupsQuery: IRequest<IHandlerResult<GetAllUserGroupsQueryResponse>>;
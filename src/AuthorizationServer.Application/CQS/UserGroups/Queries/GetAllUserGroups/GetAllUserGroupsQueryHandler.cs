﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.UserGroups;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.UserGroups.Queries.GetAllUserGroups;

public class GetAllUserGroupsQueryHandler: RequestHandlerBase<GetAllUserGroupsQuery, GetAllUserGroupsQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetAllUserGroupsQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }
    
    public override async Task<IHandlerResult<GetAllUserGroupsQueryResponse>> Handle(GetAllUserGroupsQuery query, CancellationToken cancellationToken)
    {
        var userGroupEntities = await _unitOfWork.UserGroupRepository.GetAllWithDetailsAsync(cancellationToken);
        return Data(new GetAllUserGroupsQueryResponse(_mapper.Map<List<UserGroupWithRolesDto>>(userGroupEntities)));
    }
}
﻿using System.Security.Claims;

namespace AuthorizationServer.Application.CQS.Account.Queries.Login;

public class LoginQueryResponse
{
    public IList<Claim> Claims { get; set; }
}
﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Account.Queries.Login;

public record LoginQuery(string Username, string Password): IRequest<IHandlerResult<LoginQueryResponse>>;
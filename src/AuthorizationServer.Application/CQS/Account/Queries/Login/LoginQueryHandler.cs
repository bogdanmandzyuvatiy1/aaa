﻿using System.Security.Claims;
using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Infrastructure.Interfaces;

namespace AuthorizationServer.Application.CQS.Account.Queries.Login;

public class LoginQueryHandler: RequestHandlerBase<LoginQuery, LoginQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;

    public LoginQueryHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    
    public override async Task<IHandlerResult<LoginQueryResponse>> Handle(LoginQuery query, CancellationToken cancellationToken)
    {
        var user = (await _unitOfWork.UserRepository.GetAsync(u =>
            u.Username == query.Username && u.Password == query.Password)).SingleOrDefault();

        if (user != null)
        {
            var claims = new List<Claim>
            {
                new(ClaimTypes.Name, query.Username)
            };
            return Data(new LoginQueryResponse {Claims = claims});
        }

        return NotFound<LoginQueryResponse>(Constants.ErrorMessages.LoginFailedError);
    }
}
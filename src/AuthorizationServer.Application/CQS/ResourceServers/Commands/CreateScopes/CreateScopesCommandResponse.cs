﻿using AuthorizationServer.Application.DTOs.Scopes;

namespace AuthorizationServer.Application.CQS.ResourceServers.Commands.CreateScopes;

public class CreateScopesCommandResponse
{
    public CreateScopesCommandResponse(IList<ScopeDto> scopeDtos)
    {
        ScopeDtos = scopeDtos;
    }

    public IList<ScopeDto> ScopeDtos { get; set; }
}
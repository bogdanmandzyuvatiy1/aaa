﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.ResourceServers.Commands.CreateScopes;

public record CreateScopesCommand(
    IEnumerable<string> ScopeNames, string ResourceServerName): IRequest<IHandlerResult<CreateScopesCommandResponse>>;
﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Scopes;
using AuthorizationServer.Domain.ResourceServers;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.ResourceServers.Commands.CreateScopes;

public class CreateScopesCommandHandler : RequestHandlerBase<CreateScopesCommand, CreateScopesCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public CreateScopesCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<CreateScopesCommandResponse>> Handle(CreateScopesCommand command,
        CancellationToken cancellationToken)
    {
        var resourceServerEntity = await _unitOfWork.ResourceServerRepository.GetAsync(command.ResourceServerName);

        if (resourceServerEntity == null)
        {
            return await CreateResourceServer(command, cancellationToken);
        }

        return await CreateScopes(resourceServerEntity, command.ScopeNames, cancellationToken);
    }

    private async Task<IHandlerResult<CreateScopesCommandResponse>> CreateResourceServer(CreateScopesCommand command,
        CancellationToken cancellationToken)
    {
        var resourceServerCreationResult = ResourceServer.Create(command.ResourceServerName, command.ScopeNames);
        if (!resourceServerCreationResult.IsSuccess)
        {
            return Conflict<CreateScopesCommandResponse>(resourceServerCreationResult.Message);
        }

        var resourceServer = resourceServerCreationResult.Resource;
        var resourceServerEntity = _mapper.Map<ResourceServerEntity>(resourceServer);
        await _unitOfWork.ResourceServerRepository.AddAsync(resourceServerEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new CreateScopesCommandResponse( _mapper.Map<List<ScopeDto>>(resourceServerEntity.Scopes)));
    }

    private async Task<IHandlerResult<CreateScopesCommandResponse>> CreateScopes(
        ResourceServerEntity resourceServerEntity, IEnumerable<string> scopeNames,
        CancellationToken cancellationToken)
    {
        var resourceServer = _mapper.Map<ResourceServer>(resourceServerEntity);
        var result = resourceServer.AddScopes(scopeNames);
        if (!result.IsSuccess)
        {
            return Conflict<CreateScopesCommandResponse>(result.Message);
        }
        _mapper.Map(result.Resource, resourceServerEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new CreateScopesCommandResponse (_mapper.Map<List<ScopeDto>>(resourceServerEntity.Scopes)));
    }
}
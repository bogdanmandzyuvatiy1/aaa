﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.ResourceServers.Queries.GetScopesByResourceServerId;

public record GetScopesByResourceServerIdQuery(int ResourceServerId) : IRequest<IHandlerResult<GetScopesByResourceServerIdQueryResponse>>;
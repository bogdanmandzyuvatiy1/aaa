﻿using AuthorizationServer.Application.DTOs.Scopes;

namespace AuthorizationServer.Application.CQS.ResourceServers.Queries.GetScopesByResourceServerId;

public class GetScopesByResourceServerIdQueryResponse
{
    public GetScopesByResourceServerIdQueryResponse(IList<ScopeDto> scopeDtos)
    {
        ScopeDtos = scopeDtos;
    }

    public IList<ScopeDto> ScopeDtos { get; set; }
}
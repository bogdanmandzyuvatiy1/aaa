﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Scopes;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.ResourceServers.Queries.GetScopesByResourceServerId;

public class GetScopesByResourceServerIdQueryHandler : RequestHandlerBase<GetScopesByResourceServerIdQuery, GetScopesByResourceServerIdQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetScopesByResourceServerIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<GetScopesByResourceServerIdQueryResponse>> Handle(GetScopesByResourceServerIdQuery query, CancellationToken cancellationToken)
    {
        var scopeEntities = await _unitOfWork.ScopeRepository.GetScopesByResourceServerIdAsync(query.ResourceServerId);
        return Data(new GetScopesByResourceServerIdQueryResponse (_mapper.Map<List<ScopeDto>>(scopeEntities)));
    }
}
﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetUserById;

public record GetUserByIdQuery(int Id): IRequest<IHandlerResult<GetUserByIdQueryResponse>>;
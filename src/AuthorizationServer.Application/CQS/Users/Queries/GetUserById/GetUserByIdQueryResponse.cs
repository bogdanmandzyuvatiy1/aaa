﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetUserById;

public class GetUserByIdQueryResponse
{
    public UserDto UserDto { get; set; }

    public GetUserByIdQueryResponse(UserDto userDto)
    {
        UserDto = userDto;
    }
}


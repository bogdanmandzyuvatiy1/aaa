﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetAllUsers;

public class GetAllUsersQueryHandler: RequestHandlerBase<GetAllUsersQuery, GetAllUsersQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetAllUsersQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }
    
    public override async Task<IHandlerResult<GetAllUsersQueryResponse>> Handle(GetAllUsersQuery query, CancellationToken cancellationToken)
    {
        var userEntities = await _unitOfWork.UserRepository.GetAllAsync(cancellationToken);
        return Data(new GetAllUsersQueryResponse(_mapper.Map<List<UserDto>>(userEntities)));
    }
}
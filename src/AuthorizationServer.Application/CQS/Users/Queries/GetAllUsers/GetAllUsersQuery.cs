﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetAllUsers;

public record GetAllUsersQuery : IRequest<IHandlerResult<GetAllUsersQueryResponse>>;
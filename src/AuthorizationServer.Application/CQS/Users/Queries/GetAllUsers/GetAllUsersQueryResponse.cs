﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetAllUsers;

public class GetAllUsersQueryResponse
{
    public IList<UserDto> UserDtos { get; set; }

    public GetAllUsersQueryResponse(IList<UserDto> userDtos)
    {
        UserDtos = userDtos;
    }
}


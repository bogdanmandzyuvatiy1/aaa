﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Scopes;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetScopesByUserId;

public class GetScopesByUserIdQueryHandler: RequestHandlerBase<GetScopesByUserIdQuery, GetScopesByUserIdQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetScopesByUserIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }
    
    public override async Task<IHandlerResult<GetScopesByUserIdQueryResponse>> Handle(GetScopesByUserIdQuery query, CancellationToken cancellationToken)
    {
        var scopeEntities = await _unitOfWork.ScopeRepository.GetScopesByUserIdAsync(query.UserId);
        return Data(new GetScopesByUserIdQueryResponse (_mapper.Map<List<ScopeDto>>(scopeEntities)));
    }
}
﻿using AuthorizationServer.Application.DTOs.Scopes;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetScopesByUserId;

public class GetScopesByUserIdQueryResponse
{
    public IList<ScopeDto> ScopeDtos { get; set; }

    public GetScopesByUserIdQueryResponse(IList<ScopeDto> scopeDtos)
    {
        ScopeDtos = scopeDtos;
    }
}
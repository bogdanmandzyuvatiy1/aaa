﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Users.Queries.GetScopesByUserId;

public record GetScopesByUserIdQuery(int UserId): IRequest<IHandlerResult<GetScopesByUserIdQueryResponse>>;
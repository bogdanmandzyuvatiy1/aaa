﻿using AuthorizationServer.Domain.Users.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Interfaces;

namespace AuthorizationServer.Application.CQS.Users.Rules;

public class UsernameUniquenessChecker : IUserUniquenessChecker
{
    private readonly IUnitOfWork _unitOfWork;

    public UsernameUniquenessChecker(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    public bool IsUnique(string username, int id = 0)
    {
        if (id == 0)
        {
            return !_unitOfWork.UserRepository.Exists(username);
        }

        return !_unitOfWork.UserRepository.ExistsWithAnotherId(username, id);
    }
}
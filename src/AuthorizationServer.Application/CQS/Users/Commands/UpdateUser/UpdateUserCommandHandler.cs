﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Commands.UpdateUser;

public class UpdateUserCommandHandler : RequestHandlerBase<UpdateUserCommand, UpdateUserCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UpdateUserCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<UpdateUserCommandResponse>> Handle(UpdateUserCommand command,
        CancellationToken cancellationToken)
    {
        var userEntity = await _unitOfWork.UserRepository.GetAsync(command.Id, cancellationToken);
        if (userEntity == null)
        {
            return NotFound<UpdateUserCommandResponse>(ErrorMessages.UserWithSuchIdNotFound);
        }

        var user = _mapper.Map<User>(userEntity);
        var result = user.Update(user.UserGroupId, command.FirstName, command.LastName, command.Status,
            command.EmployeeNumber, command.BadgeNumber, command.HireDate, command.TerminationDate);

        if (!result.IsSuccess)
        {
            return Conflict<UpdateUserCommandResponse>(result.Message);
        }

        _mapper.Map(result.Resource, userEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new UpdateUserCommandResponse (_mapper.Map<UserDto>(userEntity)));
    }
}
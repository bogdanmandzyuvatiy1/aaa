﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Users.Commands.UpdateUser;

public record UpdateUserCommand(int Id, string FirstName, string? LastName, bool? Status, string? EmployeeNumber,
    string? BadgeNumber, DateTime? HireDate, DateTime? TerminationDate) : IRequest<IHandlerResult<UpdateUserCommandResponse>>;
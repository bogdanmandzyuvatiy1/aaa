﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Commands.UpdateUser;

public class UpdateUserCommandResponse
{
    public UpdateUserCommandResponse(UserDto userDto)
    {
        UserDto = userDto;
    }

    public UserDto UserDto { get; set; }
}
﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUser;

public class PartiallyUpdateUserCommandResponse
{
    public PartiallyUpdateUserCommandResponse(UserDto userDto)
    {
        UserDto = userDto;
    }

    public UserDto UserDto { get; set; }
}
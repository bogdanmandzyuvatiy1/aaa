﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Users;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;

namespace AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUser;

public record PartiallyUpdateUserCommand
    (int Id, JsonPatchDocument<UpdateUserDto> PatchDoc) : IRequest<IHandlerResult<PartiallyUpdateUserCommandResponse>>;
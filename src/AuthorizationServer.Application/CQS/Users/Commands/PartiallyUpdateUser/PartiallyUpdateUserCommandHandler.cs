﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUser;

public class
    PartiallyUpdateUserCommandHandler : RequestHandlerBase<PartiallyUpdateUserCommand,
        PartiallyUpdateUserCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public PartiallyUpdateUserCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<PartiallyUpdateUserCommandResponse>> Handle(
        PartiallyUpdateUserCommand command,
        CancellationToken cancellationToken)
    {
        var userEntity = await _unitOfWork.UserRepository.GetAsync(command.Id, cancellationToken);
        if (userEntity == null)
        {
            return NotFound<PartiallyUpdateUserCommandResponse>(ErrorMessages.UserWithSuchIdNotFound);
        }

        var userDto = _mapper.Map<UpdateUserDto>(userEntity);
        command.PatchDoc.ApplyTo(userDto);

        var user = _mapper.Map<User>(userEntity);
        var result = user.Update(user.UserGroupId, userDto.FirstName, userDto.LastName, userDto.Status,
            userDto.EmployeeNumber, userDto.BadgeNumber, userDto.HireDate, userDto.TerminationDate);

        if (!result.IsSuccess)
        {
            return Conflict<PartiallyUpdateUserCommandResponse>(result.Message);
        }

        _mapper.Map(result.Resource, userEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new PartiallyUpdateUserCommandResponse(_mapper.Map<UserDto>(userEntity)));
    }
}
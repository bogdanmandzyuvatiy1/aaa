﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Commands.CreateUser;

public class CreateUserCommandResponse
{
    public CreateUserCommandResponse(UserDto userDto)
    {
        UserDto = userDto;
    }

    public UserDto UserDto { get; set; }
}
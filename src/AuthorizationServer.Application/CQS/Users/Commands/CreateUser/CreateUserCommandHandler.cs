﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Domain.Users.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Commands.CreateUser;

public class CreateUserCommandHandler : RequestHandlerBase<CreateUserCommand, CreateUserCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IUserUniquenessChecker _userUniquenessChecker;

    public CreateUserCommandHandler(IUnitOfWork unitOfWork, IMapper mapper,
        IUserUniquenessChecker userUniquenessChecker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _userUniquenessChecker = userUniquenessChecker;
    }

    public override async Task<IHandlerResult<CreateUserCommandResponse>> Handle(CreateUserCommand command,
        CancellationToken cancellationToken)
    {
        if (command.UserGroupId != null)
        {
            var userGroupExists= !_unitOfWork.UserGroupRepository.Exists(command.UserGroupId);
            if (userGroupExists)
            {
                return NotFound<CreateUserCommandResponse>(ErrorMessages.UserGroupWithSuchIdNotFound);
            } 
        }
        
        var result = User.RegisterUser(command.Username, command.UserGroupId, command.Password, command.FirstName, _userUniquenessChecker, command.LastName,
            command.Status, command.EmployeeNumber, command.BadgeNumber, command.HireDate, command.TerminationDate);
        if (!result.IsSuccess)
        {
            return Conflict<CreateUserCommandResponse>(result.Message);
        }

        var userEntity = await _unitOfWork.UserRepository.AddAsync(_mapper.Map<UserEntity>(result.Resource));
        await _unitOfWork.SaveAsync(cancellationToken);
        return Created(new CreateUserCommandResponse (_mapper.Map<UserDto>(userEntity)));
    }
}
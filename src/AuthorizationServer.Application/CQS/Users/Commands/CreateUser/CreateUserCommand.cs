﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Users.Commands.CreateUser;

public record CreateUserCommand(string Username, int? UserGroupId, string Password, string FirstName,
    string? LastName = null, bool? Status = null, string? EmployeeNumber = null, string? BadgeNumber = null,
    DateTime? HireDate = null, DateTime? TerminationDate = null) : IRequest<IHandlerResult<CreateUserCommandResponse>>;
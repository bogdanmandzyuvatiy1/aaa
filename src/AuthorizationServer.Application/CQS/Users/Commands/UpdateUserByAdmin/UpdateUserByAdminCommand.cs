﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Users.Commands.UpdateUserByAdmin;

public record UpdateUserByAdminCommand(int Id, int? UserGroupId, string FirstName, string? LastName,
    bool? Status, string? EmployeeNumber, string? BadgeNumber, DateTime? HireDate,
    DateTime? TerminationDate) : IRequest<IHandlerResult<UpdateUserByAdminCommandResponse>>;
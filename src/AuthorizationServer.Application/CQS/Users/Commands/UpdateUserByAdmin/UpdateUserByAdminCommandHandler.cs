﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Commands.UpdateUserByAdmin;

public class UpdateUserByAdminCommandHandler : RequestHandlerBase<UpdateUserByAdminCommand, UpdateUserByAdminCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public UpdateUserByAdminCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<UpdateUserByAdminCommandResponse>> Handle(UpdateUserByAdminCommand command,
        CancellationToken cancellationToken)
    {
        var userEntity = await _unitOfWork.UserRepository.GetAsync(command.Id, cancellationToken);
        if (userEntity == null)
        {
            return NotFound<UpdateUserByAdminCommandResponse>(ErrorMessages.UserWithSuchIdNotFound);
        }

        if (command.UserGroupId != null)
        {
            var userGroupExists= !_unitOfWork.UserGroupRepository.Exists(command.UserGroupId);
            if (userGroupExists)
            {
                return NotFound<UpdateUserByAdminCommandResponse>(ErrorMessages.UserGroupWithSuchIdNotFound);
            } 
        }
        
        var user = _mapper.Map<User>(userEntity);
        var result = user.Update(command.UserGroupId, command.FirstName, command.LastName, command.Status,
            command.EmployeeNumber, command.BadgeNumber, command.HireDate, command.TerminationDate);

        if (!result.IsSuccess)
        {
            return Conflict<UpdateUserByAdminCommandResponse>(result.Message);
        }

        _mapper.Map(result.Resource, userEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new UpdateUserByAdminCommandResponse (_mapper.Map<UserDto>(userEntity)));
    }
}
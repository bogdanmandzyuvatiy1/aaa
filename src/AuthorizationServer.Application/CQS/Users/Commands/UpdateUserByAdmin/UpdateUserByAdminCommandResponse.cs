﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Commands.UpdateUserByAdmin;

public class UpdateUserByAdminCommandResponse
{
    public UpdateUserByAdminCommandResponse(UserDto userDto)
    {
        UserDto = userDto;
    }

    public UserDto UserDto { get; set; }
}
﻿using AuthorizationServer.Application.DTOs.Users;

namespace AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUserByAdmin;

public class PartiallyUpdateUserByAdminCommandResponse
{
    public PartiallyUpdateUserByAdminCommandResponse(UserDto userDto)
    {
        UserDto = userDto;
    }

    public UserDto UserDto { get; set; }
}
﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Users;
using MediatR;
using Microsoft.AspNetCore.JsonPatch;

namespace AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUserByAdmin;

public record PartiallyUpdateUserByAdminCommand(int Id, JsonPatchDocument<UpdateUserByAdminDto> PatchDoc)
    : IRequest<IHandlerResult<PartiallyUpdateUserByAdminCommandResponse>>;
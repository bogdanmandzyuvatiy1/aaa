﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Users;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Users.Commands.PartiallyUpdateUserByAdmin;

public class
    PartiallyUpdateUserByAdminCommandHandler : RequestHandlerBase<PartiallyUpdateUserByAdminCommand, PartiallyUpdateUserByAdminCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public PartiallyUpdateUserByAdminCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }
    
    public override async Task<IHandlerResult<PartiallyUpdateUserByAdminCommandResponse>> Handle(PartiallyUpdateUserByAdminCommand command,
        CancellationToken cancellationToken)
    {
        var userEntity = await _unitOfWork.UserRepository.GetAsync(command.Id, cancellationToken);
        if (userEntity == null)
        {
            return NotFound<PartiallyUpdateUserByAdminCommandResponse>(ErrorMessages.UserWithSuchIdNotFound);
        }

        var userDto = _mapper.Map<UpdateUserByAdminDto>(userEntity);
        command.PatchDoc.ApplyTo(userDto);
        
        if (userDto.UserGroupId != null)
        {
            var userGroupExists= !_unitOfWork.UserGroupRepository.Exists(userDto.UserGroupId);
            if (userGroupExists)
            {
                return NotFound<PartiallyUpdateUserByAdminCommandResponse>(ErrorMessages.UserGroupWithSuchIdNotFound);
            } 
        }
        
        var user = _mapper.Map<User>(userEntity);
        var result = user.Update( userDto.UserGroupId, userDto.FirstName,
            userDto.LastName, userDto.Status, userDto.EmployeeNumber, userDto.BadgeNumber, userDto.HireDate,
            userDto.TerminationDate);
        
        if (!result.IsSuccess)
        {
            return Conflict<PartiallyUpdateUserByAdminCommandResponse>(result.Message);
        }
        
        _mapper.Map(result.Resource, userEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new PartiallyUpdateUserByAdminCommandResponse(_mapper.Map<UserDto>(userEntity)));
    }
}
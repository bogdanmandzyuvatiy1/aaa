﻿using AuthorizationServer.Application.DTOs.Scopes;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetScopesByRoleId;

public class GetScopesByRoleIdQueryResponse
{
    public GetScopesByRoleIdQueryResponse(List<ScopeDto> scopeDtos)
    {
        ScopeDtos = scopeDtos;
    }
    public List<ScopeDto> ScopeDtos { get; set; }
}
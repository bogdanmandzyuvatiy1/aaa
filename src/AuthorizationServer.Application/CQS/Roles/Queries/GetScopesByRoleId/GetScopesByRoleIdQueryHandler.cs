﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Scopes;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetScopesByRoleId;

public class GetScopesByRoleIdQueryHandler : RequestHandlerBase<GetScopesByRoleIdQuery, GetScopesByRoleIdQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetScopesByRoleIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<GetScopesByRoleIdQueryResponse>> Handle(GetScopesByRoleIdQuery query, CancellationToken cancellationToken)
    {
        var scopeEntities = await _unitOfWork.ScopeRepository.GetScopesByRoleIdAsync(query.RoleId);
        return Data(new GetScopesByRoleIdQueryResponse(_mapper.Map<List<ScopeDto>>(scopeEntities)));
    }
}
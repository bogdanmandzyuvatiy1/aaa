﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetScopesByRoleId;

public record GetScopesByRoleIdQuery(int RoleId) : IRequest<IHandlerResult<GetScopesByRoleIdQueryResponse>>;

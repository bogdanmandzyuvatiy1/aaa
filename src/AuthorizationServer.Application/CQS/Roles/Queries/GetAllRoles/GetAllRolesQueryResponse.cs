﻿using AuthorizationServer.Application.DTOs.Roles;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetAllRoles;

public class GetAllRolesQueryResponse
{
    public GetAllRolesQueryResponse(List<RoleWithScopesDto> roleDtos)
    {
        RoleDtos = roleDtos;
    }

    public List<RoleWithScopesDto> RoleDtos { get; set; }
}
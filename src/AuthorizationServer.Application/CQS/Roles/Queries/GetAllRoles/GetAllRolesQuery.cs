﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetAllRoles;

public record GetAllRolesQuery : IRequest<IHandlerResult<GetAllRolesQueryResponse>>;
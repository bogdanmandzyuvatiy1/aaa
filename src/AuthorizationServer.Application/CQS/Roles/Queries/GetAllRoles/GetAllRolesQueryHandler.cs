﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.DTOs.Roles;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetAllRoles;

public class GetAllRolesQueryHandler : RequestHandlerBase<GetAllRolesQuery, GetAllRolesQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetAllRolesQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<GetAllRolesQueryResponse>> Handle(GetAllRolesQuery query, CancellationToken cancellationToken)
    {
        var roleEntities = await _unitOfWork.RoleRepository.GetAllWithDetailsAsync(cancellationToken);
        return Data(new GetAllRolesQueryResponse(_mapper.Map<List<RoleWithScopesDto>>(roleEntities)));
    }
}
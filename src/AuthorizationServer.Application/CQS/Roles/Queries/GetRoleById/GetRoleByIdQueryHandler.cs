﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Roles;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetRoleById;

public class GetRoleByIdQueryHandler : RequestHandlerBase<GetRoleByIdQuery, GetRoleByIdQueryResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public GetRoleByIdQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<GetRoleByIdQueryResponse>> Handle(GetRoleByIdQuery query,
        CancellationToken cancellationToken)
    {
        var roleEntity = await _unitOfWork.RoleRepository.GetWithDetailsAsync(query.RoleId, cancellationToken);
        return roleEntity == null
            ? NotFound<GetRoleByIdQueryResponse>(ErrorMessages.RoleWithSuchIdNotFound)
            : Data(new GetRoleByIdQueryResponse(_mapper.Map<RoleWithScopesDto>(roleEntity)));
    }
}
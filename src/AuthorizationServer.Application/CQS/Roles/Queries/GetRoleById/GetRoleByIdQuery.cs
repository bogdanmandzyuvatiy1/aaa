﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetRoleById;

public record GetRoleByIdQuery(int RoleId) : IRequest<IHandlerResult<GetRoleByIdQueryResponse>>;
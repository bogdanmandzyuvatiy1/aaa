﻿using AuthorizationServer.Application.DTOs.Roles;

namespace AuthorizationServer.Application.CQS.Roles.Queries.GetRoleById;

public class GetRoleByIdQueryResponse
{
    public GetRoleByIdQueryResponse(RoleWithScopesDto roleDto)
    {
        RoleDto = roleDto;
    }

    public RoleWithScopesDto RoleDto { get; set; }
}
﻿using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Interfaces;

namespace AuthorizationServer.Application.CQS.Roles.Rules;

public class RoleNameUniquenessChecker : IRoleNameUniquenessChecker
{
    private readonly IUnitOfWork _unitOfWork;

    public RoleNameUniquenessChecker(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }
    public bool IsUnique(string roleName, int id = 0)
    {
        if (id != 0)
        {
            return !_unitOfWork.RoleRepository.ExistsWithAnotherId(roleName, id);
        }

        return !_unitOfWork.RoleRepository.Exists(roleName);
    }
}
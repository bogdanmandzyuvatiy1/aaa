﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Roles;
using AuthorizationServer.Domain.Roles;
using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Commands.CreateRole;

public class CreateRoleCommandHandler : RequestHandlerBase<CreateRoleCommand, CreateRoleCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IRoleNameUniquenessChecker _roleNameUniquenessChecker;

    public CreateRoleCommandHandler(IUnitOfWork unitOfWork, IMapper mapper, IRoleNameUniquenessChecker roleNameUniquenessChecker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _roleNameUniquenessChecker = roleNameUniquenessChecker;
    }

    public override async Task<IHandlerResult<CreateRoleCommandResponse>> Handle(CreateRoleCommand command,
        CancellationToken cancellationToken)
    {
        var nonExistingScopesIds = command.ScopeIds.Where(sid => !_unitOfWork.ScopeRepository.Exists(sid)).ToList();
        if (nonExistingScopesIds.Any())
        {
            return NotFound<CreateRoleCommandResponse>(ErrorMessages.ScopesWithSuchIdsNotFound + $"[{ string.Join("], [", nonExistingScopesIds)}]");
        }
        
        var role = Role.Create(command.RoleName, command.ScopeIds, _roleNameUniquenessChecker);
        if (!role.IsSuccess)
        {
            return Conflict<CreateRoleCommandResponse>(role.Message);
        }

        var roleEntity = _mapper.Map<RoleEntity>(role.Resource);
        await _unitOfWork.RoleRepository.AddAsync(roleEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Created(new CreateRoleCommandResponse(_mapper.Map<RoleDto>(roleEntity)));
    }
}
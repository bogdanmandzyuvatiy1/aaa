﻿using AuthorizationServer.Application.DTOs.Roles;

namespace AuthorizationServer.Application.CQS.Roles.Commands.CreateRole;

public class CreateRoleCommandResponse
{
    public CreateRoleCommandResponse(RoleDto roleDto)
    {
        RoleDto = roleDto;
    }
    public RoleDto RoleDto { get; set; }
}
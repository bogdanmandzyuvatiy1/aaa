﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Commands.CreateRole;

public record CreateRoleCommand(string RoleName, List<int> ScopeIds) 
    : IRequest<IHandlerResult<CreateRoleCommandResponse>>;
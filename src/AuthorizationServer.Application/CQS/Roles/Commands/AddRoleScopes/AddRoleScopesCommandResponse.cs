﻿namespace AuthorizationServer.Application.CQS.Roles.Commands.AddRoleScopes;

public class AddRoleScopesCommandResponse
{
    public AddRoleScopesCommandResponse(List<int> scopeIds)
    {
        ScopeIds = scopeIds;
    }
    public List<int> ScopeIds { get; set; }
}
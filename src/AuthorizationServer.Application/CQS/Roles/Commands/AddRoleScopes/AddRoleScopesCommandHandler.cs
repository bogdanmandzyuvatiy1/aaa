﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Domain.Roles;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Commands.AddRoleScopes;

public class AddRoleScopesCommandHandler : RequestHandlerBase<AddRoleScopesCommand, AddRoleScopesCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;

    public AddRoleScopesCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<AddRoleScopesCommandResponse>> Handle(AddRoleScopesCommand command,
        CancellationToken cancellationToken)
    {
        var roleEntity = await _unitOfWork.RoleRepository.GetWithDetailsAsync(command.RoleId, cancellationToken);
        if (roleEntity == null)
        {
            return NotFound<AddRoleScopesCommandResponse>(ErrorMessages.RoleWithSuchIdNotFound);
        }

        var nonExistingScopesIds = command.ScopeIds.Where(sid => !_unitOfWork.ScopeRepository.Exists(sid)).ToList();
        if (nonExistingScopesIds.Any())
        {
            return NotFound<AddRoleScopesCommandResponse>(ErrorMessages.ScopesWithSuchIdsNotFound + $"[{ string.Join("], [", nonExistingScopesIds)}]");
        }

        var role = _mapper.Map<Role>(roleEntity);
        var result = role.AddRoleScopes(command.ScopeIds);
        if (!result.IsSuccess)
        {
            return Conflict<AddRoleScopesCommandResponse>(result.Message);
        }

        var res = _mapper.Map(result.Resource, roleEntity);
        await _unitOfWork.SaveAsync(cancellationToken);

        return Created(new AddRoleScopesCommandResponse(res.RoleScopes.Select(rs => rs.Id).ToList()));
    }
}
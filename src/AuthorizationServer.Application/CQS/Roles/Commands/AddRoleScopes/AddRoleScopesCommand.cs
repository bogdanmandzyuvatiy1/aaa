﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Commands.AddRoleScopes;

public record AddRoleScopesCommand(int RoleId, List<int> ScopeIds) : IRequest<IHandlerResult<AddRoleScopesCommandResponse>>;
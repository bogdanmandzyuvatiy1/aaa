﻿using AuthorizationServer.Application.DTOs.Roles;

namespace AuthorizationServer.Application.CQS.Roles.Commands.DeleteRole;

public class DeleteRoleCommandResponse
{
    public DeleteRoleCommandResponse(RoleDto roleDto)
    {
        RoleDto = roleDto;
    }
    public RoleDto RoleDto { get; set; }
}
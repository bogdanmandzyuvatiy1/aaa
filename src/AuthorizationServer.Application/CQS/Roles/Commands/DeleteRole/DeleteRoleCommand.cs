﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Commands.DeleteRole;

public record DeleteRoleCommand(int RoleId) : IRequest<IHandlerResult<DeleteRoleCommandResponse>>;
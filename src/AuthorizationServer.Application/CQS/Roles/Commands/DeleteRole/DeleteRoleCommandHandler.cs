﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Roles;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Commands.DeleteRole;

public class DeleteRoleCommandHandler : RequestHandlerBase<DeleteRoleCommand, DeleteRoleCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    public DeleteRoleCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
    }

    public override async Task<IHandlerResult<DeleteRoleCommandResponse>> Handle(DeleteRoleCommand command,
        CancellationToken cancellationToken)
    {
        var roleEntity = await _unitOfWork.RoleRepository.GetWithDetailsAsync(command.RoleId, cancellationToken);
        if (roleEntity == null)
        {
            return NotFound<DeleteRoleCommandResponse>(ErrorMessages.RoleWithSuchIdNotFound);
        }

        await _unitOfWork.RoleRepository.DeleteAsync(roleEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new DeleteRoleCommandResponse(_mapper.Map<RoleDto>(roleEntity)));
    }
}
﻿using Art3mis.Common.Core.HandleResults.Abstractions;
using MediatR;

namespace AuthorizationServer.Application.CQS.Roles.Commands.UpdateRole;

public record UpdateRoleCommand(int RoleId, string RoleName, IEnumerable<int> ScopesIds) 
    : IRequest<IHandlerResult<UpdateRoleCommandResponse>>;
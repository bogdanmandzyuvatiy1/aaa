﻿using Art3mis.Common.Core.HandleResults;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.DTOs.Roles;
using AuthorizationServer.Domain.Roles;
using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Infrastructure.Interfaces;
using AutoMapper;

namespace AuthorizationServer.Application.CQS.Roles.Commands.UpdateRole;

public class UpdateRoleCommandHandler : RequestHandlerBase<UpdateRoleCommand, UpdateRoleCommandResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IRoleNameUniquenessChecker _roleNameUniquenessChecker;

    public UpdateRoleCommandHandler(IUnitOfWork unitOfWork, IMapper mapper,
        IRoleNameUniquenessChecker roleUniquenessChecker)
    {
        _unitOfWork = unitOfWork;
        _mapper = mapper;
        _roleNameUniquenessChecker = roleUniquenessChecker;
    }

    public override async Task<IHandlerResult<UpdateRoleCommandResponse>> Handle(UpdateRoleCommand command,
        CancellationToken cancellationToken)
    {
        var roleEntity = await _unitOfWork.RoleRepository.GetWithDetailsAsync(command.RoleId, cancellationToken);
        if (roleEntity == null)
        {
            return NotFound<UpdateRoleCommandResponse>(ErrorMessages.ResourceServerWithThisIdNotFound);
        }

        var nonExistingScopesIds = command.ScopesIds.Where(sid => !_unitOfWork.ScopeRepository.Exists(sid)).ToList();
        if (nonExistingScopesIds.Any())
        {
            return NotFound<UpdateRoleCommandResponse>(ErrorMessages.ScopesWithSuchIdsNotFound + $"[{ string.Join("], [", nonExistingScopesIds)}]");
        }

        var role = _mapper.Map<Role>(roleEntity);
        var result = role.Update(command.RoleName, command.ScopesIds, _roleNameUniquenessChecker);

        if (!result.IsSuccess)
        {
            return Conflict<UpdateRoleCommandResponse>(result.Message);
        }

        _mapper.Map(result.Resource, roleEntity);
        await _unitOfWork.SaveAsync(cancellationToken);
        return Data(new UpdateRoleCommandResponse(_mapper.Map<RoleDto>(roleEntity)));
    }
}
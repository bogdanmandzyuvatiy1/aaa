﻿using AuthorizationServer.Application.DTOs.Roles;

namespace AuthorizationServer.Application.CQS.Roles.Commands.UpdateRole;

public class UpdateRoleCommandResponse
{
    public UpdateRoleCommandResponse(RoleDto role)
    {
        RoleDto = role;
    }
    public RoleDto RoleDto { get; set; }
}
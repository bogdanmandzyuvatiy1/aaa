﻿namespace AuthorizationServer.Domain.Roles.RoleScopes;
public class RoleScope : IEquatable<RoleScope>
{
    public int Id { get; init; }
    public int ScopeId { get; init; }

    public RoleScope(int scopeId)
    {
        ScopeId = scopeId;
    }
    public static RoleScope Create(int scopeId)
    {
        return new RoleScope(scopeId);
    }
    
    public bool Equals(RoleScope? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return ScopeId == other.ScopeId;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((RoleScope) obj);
    }

    public override int GetHashCode()
    {
        return ScopeId;
    }
}
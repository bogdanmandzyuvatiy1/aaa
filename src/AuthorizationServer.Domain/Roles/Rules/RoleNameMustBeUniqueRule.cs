﻿using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Domain.SeedWork;

namespace AuthorizationServer.Domain.Roles.Rules;

public class RoleNameMustBeUniqueRule : IBusinessRule
{
    private readonly IRoleNameUniquenessChecker _roleNameUnique;
    private readonly string _roleName;
    private readonly int _id;

    public RoleNameMustBeUniqueRule(IRoleNameUniquenessChecker roleNameUnique, string roleName, int id = 0)
    {
        _roleNameUnique = roleNameUnique;
        _roleName = roleName;
        _id = id;
    }


    public IRuleCheckResult IsBroken()
    {
        return !_roleNameUnique.IsUnique(_roleName, _id)
            ? new RuleCheckResult(false, ErrorMessages.RoleAlreadyExists)
            : new RuleCheckResult(true);
    }
}
﻿namespace AuthorizationServer.Domain.Roles.Rules.Interfaces;

public interface IRoleNameUniquenessChecker
{
    bool IsUnique(string roleName, int id = 0);
}
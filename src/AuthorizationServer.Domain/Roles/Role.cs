﻿using AuthorizationServer.Domain.Roles.RoleScopes;
using AuthorizationServer.Domain.Roles.Rules;
using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Domain.SeedWork;

namespace AuthorizationServer.Domain.Roles;

public class Role : RuleChecker, IDomainModel
{
    public int Id { get; init; }
    public string RoleName { get; private set; }
    public HashSet<RoleScope> RoleScopes { get; private set; }

    private Role(string roleName, IEnumerable<int>? roleScopeIds)
    {
        RoleName = roleName;
        RoleScopes = roleScopeIds?.Select(RoleScope.Create).ToHashSet() ?? new HashSet<RoleScope>();
    }

    private Role()
    {
    }

    public static IDomainModelExecutionResult<Role> Create(string roleName, IEnumerable<int>? roleScopeIds,
        IRoleNameUniquenessChecker isRoleUnique)
    {
        var checkRulesResult = CheckDomainModelRule(new IBusinessRule[]
        {
            new RoleNameMustBeUniqueRule(isRoleUnique, roleName)
        });

        if (checkRulesResult.IsSuccess)
        {
            return new DomainModelExecutionResult<Role>
            {
                IsSuccess = true,
                Resource = new Role(roleName, roleScopeIds)
            };
        }

        return new DomainModelExecutionResult<Role>
        {
            Message = checkRulesResult.Message
        };
    }

    public IDomainModelExecutionResult<Role> AddRoleScopes(IEnumerable<int> roleScopeIds)
    {
        foreach (var roleScopeId in roleScopeIds)
        {
            RoleScopes.Add(new RoleScope(roleScopeId));
        }

        return new DomainModelExecutionResult<Role>
        {
            IsSuccess = true,
            Resource = this
        };
    }

    public IDomainModelExecutionResult<Role> Update(string roleName, IEnumerable<int>? roleScopeIds,
        IRoleNameUniquenessChecker roleNameUniquenessChecker)
    {
        var checkRulesResult = CheckDomainModelRule(new IBusinessRule[]
        {
            new RoleNameMustBeUniqueRule(roleNameUniquenessChecker, roleName, Id)
        });

        if (!checkRulesResult.IsSuccess)
        {
            return new DomainModelExecutionResult<Role>
            {
                Message = checkRulesResult.Message
            };
        }

        RoleName = roleName;
        RoleScopes = roleScopeIds?.Select(RoleScope.Create).ToHashSet() ?? new HashSet<RoleScope>();

        return new DomainModelExecutionResult<Role>
        {
            IsSuccess = true,
            Resource = this
        };
    }
}
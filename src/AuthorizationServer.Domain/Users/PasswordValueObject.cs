﻿using AuthorizationServer.Domain.SeedWork;

namespace AuthorizationServer.Domain.Users;

public class PasswordValueObject : IValueObject
{
    public string Password { get; }

    private PasswordValueObject(string password)
    {
        Password = password;
    }

    public static PasswordValueObject CreateUserPassword(string password)
    {
        return new PasswordValueObject(password);
    }
}
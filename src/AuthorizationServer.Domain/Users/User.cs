﻿using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.Users.Rules;
using AuthorizationServer.Domain.Users.Rules.Interfaces;

namespace AuthorizationServer.Domain.Users;

public class User : RuleChecker, IDomainModel
{
    public int Id { get; }
    public int? UserGroupId { get; private set; }
    public string Username { get; }
    public PasswordValueObject Password { get; }
    public string FirstName { get; private set; }
    public string? LastName { get; private set; }
    public bool? Status { get; private set; }
    public string? EmployeeNumber { get; private set; }
    public string? BadgeNumber { get; private set; }
    public DateTime? HireDate { get; private set; }
    public DateTime? TerminationDate { get; private set; }

    private User(string username, int? userGroupId, PasswordValueObject password, string firstName, string? lastName,
        bool? status,
        string? employeeNumber, string? badgeNumber, DateTime? hireDate, DateTime? terminationDate)
    {
        Username = username;
        UserGroupId = userGroupId;
        Password = password;
        FirstName = firstName;
        LastName = lastName;
        Status = status;
        EmployeeNumber = employeeNumber;
        BadgeNumber = badgeNumber;
        HireDate = hireDate;
        TerminationDate = terminationDate;
    }

    private User()
    {
    }

    public static IDomainModelExecutionResult<User> RegisterUser(string username, int? userGroupId,
        string password, string firstName, IUserUniquenessChecker isUserUnique, string? lastName = null,
        bool? status = null, string? employeeNumber = null, string? badgeNumber = null,
        DateTime? hireDate = null, DateTime? terminationDate = null)
    {
        var checkRulesResult = CheckDomainModelRule(new IBusinessRule[]
        {
            new UserUsernameMustBeUniqueRule(isUserUnique, username),
            new UserPasswordMustBeValidRule(password)
        });

        if (!checkRulesResult.IsSuccess)
        {
            return new DomainModelExecutionResult<User>
            {
                IsSuccess = false,
                Message = checkRulesResult.Message
            };
        }

        return new DomainModelExecutionResult<User>
        {
            IsSuccess = true,
            Resource = new User(username, userGroupId, PasswordValueObject.CreateUserPassword(password), firstName,
                lastName,
                status, employeeNumber, badgeNumber, hireDate, terminationDate)
        };
    }

    public IDomainModelExecutionResult<User> Update(int? userGroupId, string firstName, string? lastName, bool? status,
        string? employeeNumber, string? badgeNumber, DateTime? hireDate, DateTime? terminationDate)
    {
        UserGroupId = userGroupId;
        FirstName = firstName;
        LastName = lastName;
        Status = status;
        EmployeeNumber = employeeNumber;
        BadgeNumber = badgeNumber;
        HireDate = hireDate;
        TerminationDate = terminationDate;

        return new DomainModelExecutionResult<User>
        {
            IsSuccess = true,
            Resource = this
        };
    }
}
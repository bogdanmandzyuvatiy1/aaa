﻿using System.Text;
using System.Text.RegularExpressions;
using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.SeedWork;

namespace AuthorizationServer.Domain.Users.Rules;

public class UserPasswordMustBeValidRule : IBusinessRule
{
    private readonly string _password;

    public UserPasswordMustBeValidRule(string password)
    {
        _password = password;
    }

    public IRuleCheckResult IsBroken()
    {
        var hasNumber = new Regex(BusinessRules.HasNumberPattern);
        var hasLowerChar = new Regex(BusinessRules.HasLowerCharPattern);
        var hasUpperChar = new Regex(BusinessRules.HasUpperCharPattern);
        var hasMinimum8Chars = new Regex(BusinessRules.HasMinimum8CharsPattern);
        var hasSymbols = new Regex(BusinessRules.HasSymbolsPattern);

        var stringBuilder = new StringBuilder();
        var response = new RuleCheckResult(true, string.Empty);
        
        if (!hasMinimum8Chars.IsMatch(_password))
        {
            response.IsSuccess = false;
            response.Message += stringBuilder.AppendLine(ErrorMessages.PasswordHasMinimum8Chars);
        }
        
        if (!hasNumber.IsMatch(_password))
        {
            response.IsSuccess = false;
            response.Message += stringBuilder.AppendLine(ErrorMessages.PasswordHasNumber);
        }

        if (!hasUpperChar.IsMatch(_password))
        {
            response.IsSuccess = false;
            response.Message += stringBuilder.AppendLine(ErrorMessages.PasswordHasUpperChar);
        }
        
        if (!hasLowerChar.IsMatch(_password))
        {
            response.IsSuccess = false;
            response.Message += stringBuilder.AppendLine(ErrorMessages.PasswordHasLowerChar);
        }
        
        if (!hasSymbols.IsMatch(_password))
        {
            response.IsSuccess = false;
            response.Message += stringBuilder.AppendLine(ErrorMessages.PasswordHasSymbols);
        }
        
        response.Message = stringBuilder.ToString();
        return response;
    }
}
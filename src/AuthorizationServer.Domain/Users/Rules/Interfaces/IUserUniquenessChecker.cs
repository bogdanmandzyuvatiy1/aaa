﻿namespace AuthorizationServer.Domain.Users.Rules.Interfaces;

public interface IUserUniquenessChecker
{
    bool IsUnique(string username, int id = 0);
}
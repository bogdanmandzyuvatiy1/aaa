﻿using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.Users.Rules.Interfaces;

namespace AuthorizationServer.Domain.Users.Rules;

public class UserUsernameMustBeUniqueRule : IBusinessRule
{
    private readonly IUserUniquenessChecker _userUnique;
    private readonly string _username;
    private readonly int _id;

    public UserUsernameMustBeUniqueRule(IUserUniquenessChecker userUnique, string username, int id = 0)
    {
        _userUnique = userUnique;
        _username = username;
        _id = id;
    }

    public IRuleCheckResult IsBroken()
    {
        return !_userUnique.IsUnique(_username, _id)
            ? new RuleCheckResult(false, ErrorMessages.UsernameAlreadyExists)
            : new RuleCheckResult(true);
    }
}
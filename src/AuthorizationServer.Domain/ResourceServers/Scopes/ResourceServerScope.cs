﻿namespace AuthorizationServer.Domain.ResourceServers.Scopes;

public class ResourceServerScope : IEquatable<ResourceServerScope>
{
    public int Id { get; }
    public string ScopeName { get; }
    public int ResourceServerId { get; }

    public ResourceServerScope(string scopeName)
    {
        ScopeName = scopeName;
    }

    public ResourceServerScope(int id, string scopeName, int resourceServerId)
    {
        Id = id;
        ScopeName = scopeName;
        ResourceServerId = resourceServerId;
    }

    public bool Equals(ResourceServerScope? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return ScopeName == other.ScopeName && ResourceServerId.Equals(other.ResourceServerId);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((ResourceServerScope) obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(ScopeName, ResourceServerId);
    }
}
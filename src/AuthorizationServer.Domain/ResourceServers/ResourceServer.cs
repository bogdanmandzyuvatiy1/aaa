﻿using AuthorizationServer.Domain.ResourceServers.Scopes;
using AuthorizationServer.Domain.SeedWork;

namespace AuthorizationServer.Domain.ResourceServers;

public class ResourceServer : RuleChecker, IDomainModel
{
    public int Id { get; }
    public string ResourceServerName { get; }
    public HashSet<ResourceServerScope> Scopes { get; }

    private ResourceServer(string resourceServerName, IEnumerable<string> scopeNames)
    {
        ResourceServerName = resourceServerName;
        Scopes = new HashSet<ResourceServerScope>();
        AddScopes(scopeNames);
    }

    private ResourceServer(int id, string resourceServerName, IEnumerable<ResourceServerScope> scopes)
    {
        Id = id;
        ResourceServerName = resourceServerName;
        Scopes = scopes.ToHashSet();
    }

    public static IDomainModelExecutionResult<ResourceServer> Create(string resourceServerName,
        IEnumerable<string> scopeNames)
    {
        return new DomainModelExecutionResult<ResourceServer>
        {
            IsSuccess = true,
            Resource = new ResourceServer(resourceServerName, scopeNames)
        };
    }

    public IDomainModelExecutionResult<ResourceServer> AddScopes(IEnumerable<string> scopeNames)
    {
        foreach (var scopeName in scopeNames)
        {
            Scopes.Add(new ResourceServerScope(scopeName));
        }

        return new DomainModelExecutionResult<ResourceServer>
        {
            IsSuccess = true,
            Resource = this
        };
    }
}
﻿namespace AuthorizationServer.Domain.Constants;

public static class ErrorMessages
{
    public const string UsernameAlreadyExists = "User with such username already exists.";
    public const string RoleAlreadyExists = "Role with such a name already exists.";
    public const string UserGroupNameAlreadyExists = "User group with such a name already exists.";
    public const string PasswordHasNumber = "Password should contain at least one numeric value.";
    public const string PasswordHasUpperChar = "Password should contain at least one upper case letter.";
    public const string PasswordHasMinimum8Chars = "Password should not be lesser than 8 characters.";
    public const string PasswordHasLowerChar = "Password should contain at least one lower case letter.";
    public const string PasswordHasSymbols = "Password should contain at least one special case character.";
}
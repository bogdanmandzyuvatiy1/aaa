﻿namespace AuthorizationServer.Domain.Constants;

public static class BusinessRules
{
    public const string Empty = "";
    public const string HasSymbolsPattern = @"[!@#$%^&*()_+=\[{\]};:<>|./?,-]";
    public const string HasMinimum8CharsPattern = @".{8,}";
    public const string HasUpperCharPattern = @"[A-Z]+";
    public const string HasLowerCharPattern = @"[a-z]+";
    public const string HasNumberPattern = @"[0-9]+";
}
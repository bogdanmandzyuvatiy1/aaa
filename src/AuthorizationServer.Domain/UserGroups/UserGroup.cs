﻿using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.UserGroups.Roles;
using AuthorizationServer.Domain.UserGroups.Rules;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;

namespace AuthorizationServer.Domain.UserGroups;

public class UserGroup : RuleChecker, IDomainModel
{
    public int Id { get; init; }
    public string UserGroupName { get; private set; }
    public HashSet<UserGroupRole> UserGroupRoles { get; private set; }

    private UserGroup(string userGroupName, IEnumerable<int>? roleIds)
    {
        UserGroupName = userGroupName;
        UserGroupRoles = roleIds?.Select(UserGroupRole.Create).ToHashSet() ?? new HashSet<UserGroupRole>();
    }

    private UserGroup()
    {
    }

    public static IDomainModelExecutionResult<UserGroup> Create(string userGroupName,
        IUserGroupUniquenessChecker isUserGroupUnique, IEnumerable<int>? roleIds = null)
    {
        var checkRulesResult = CheckDomainModelRule(new IBusinessRule[]
        {
            new UserGroupNameMustBeUniqueRule(isUserGroupUnique, userGroupName)
        });

        if (checkRulesResult.IsSuccess)
        {
            return new DomainModelExecutionResult<UserGroup>
            {
                IsSuccess = true,
                Resource = new UserGroup(userGroupName, roleIds)
            };
        }

        return new DomainModelExecutionResult<UserGroup>
        {
            Message = checkRulesResult.Message
        };
        
    }

    public IDomainModelExecutionResult<UserGroup> Update(string userGroupName,
        IUserGroupUniquenessChecker isUserGroupUnique, IEnumerable<int>? roleIds = null)
    {
        var checkRulesResult = CheckDomainModelRule(new IBusinessRule[]
        {
            new UserGroupNameMustBeUniqueRule(isUserGroupUnique, userGroupName, Id)
        });
        
        if (!checkRulesResult.IsSuccess)
        {
            return new DomainModelExecutionResult<UserGroup>
            {
                IsSuccess = false,
                Message = checkRulesResult.Message
            };
        }
        
        UserGroupName = userGroupName;
        UserGroupRoles.Clear();
        UserGroupRoles = roleIds?.Select(UserGroupRole.Create).ToHashSet() ?? new HashSet<UserGroupRole>();
        
        return new DomainModelExecutionResult<UserGroup>
        {
            IsSuccess = true,
            Resource = this
        };
    }
}
﻿namespace AuthorizationServer.Domain.UserGroups.Roles;

public class UserGroupRole: IEquatable<UserGroupRole>
{
    public int Id { get; set; }
    public int RoleId { get; }

    private UserGroupRole(int roleId)
    {
        RoleId = roleId;
    }

    public static UserGroupRole Create(int id)
    {
        return new UserGroupRole(id);
    }

    public bool Equals(UserGroupRole? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return RoleId == other.RoleId;
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((UserGroupRole) obj);
    }

    public override int GetHashCode()
    {
        return RoleId;
    }
}
﻿using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;

namespace AuthorizationServer.Domain.UserGroups.Rules;

public class UserGroupNameMustBeUniqueRule : IBusinessRule
{
    private readonly IUserGroupUniquenessChecker _userGroupNameUnique;
    private readonly string _userGroupName;
    private readonly int _id;

    public UserGroupNameMustBeUniqueRule(IUserGroupUniquenessChecker userGroupNameUnique, string userGroupName, int id = 0)
    {
        _userGroupNameUnique = userGroupNameUnique;
        _userGroupName = userGroupName;
        _id = id;
    }
    public IRuleCheckResult IsBroken()
    {
        return !_userGroupNameUnique.IsUnique(_userGroupName, _id)
            ? new RuleCheckResult(false, ErrorMessages.UserGroupNameAlreadyExists)
            : new RuleCheckResult(true);
    }
}

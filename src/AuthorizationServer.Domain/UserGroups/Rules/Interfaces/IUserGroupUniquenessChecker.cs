﻿namespace AuthorizationServer.Domain.UserGroups.Rules.Interfaces;

public interface IUserGroupUniquenessChecker
{
    bool IsUnique(string username, int id = 0);
}
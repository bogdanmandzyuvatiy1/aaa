﻿namespace AuthorizationServer.Domain.SeedWork;

public class DomainModelExecutionResult<T> : IDomainModelExecutionResult<T>
{
    public T Resource { get; set; }
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
}
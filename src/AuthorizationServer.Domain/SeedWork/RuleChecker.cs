﻿namespace AuthorizationServer.Domain.SeedWork;

public abstract class RuleChecker
{
    protected static IRuleCheckResult CheckDomainModelRule(IEnumerable<IBusinessRule> rules)
    {
        foreach (var rule in rules)
        {
            var result = rule.IsBroken();
            if (!result.IsSuccess)
            {
                return result;
            }
        }
        return new RuleCheckResult(true);
    }
}
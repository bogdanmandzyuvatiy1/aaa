﻿namespace AuthorizationServer.Domain.SeedWork;

public interface IRuleCheckResult
{
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
}
﻿using AuthorizationServer.Domain.Constants;

namespace AuthorizationServer.Domain.SeedWork;

public class RuleCheckResult: IRuleCheckResult
{
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
    
    public RuleCheckResult(bool isSuccess, string message = BusinessRules.Empty)
    {
        IsSuccess = isSuccess;
        Message = message;
    }
}
﻿namespace AuthorizationServer.Domain.SeedWork;

public interface IDomainModelExecutionResult<T>
{
    public T Resource { get; set; }
    public bool IsSuccess { get; set; }
    public string Message { get; set; }
}
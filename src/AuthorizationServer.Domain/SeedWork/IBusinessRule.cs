﻿namespace AuthorizationServer.Domain.SeedWork;

public interface IBusinessRule
{
    IRuleCheckResult IsBroken();
}
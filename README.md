# "Authorization server"

## Table of Contents    
1. [Annotation](#annotation)
2. [Diagrams](#diagrams)
3. [Configuration](#configuration)


## Annotation

It is recommended to read this [annotation](#annotation) alongside with [diagrams](#diagrams).

<a id="annotation"></a>

### Solution structure:  
Solution structure described below is visualized as a '**Authorization Authentication Accounting service diagram**' [diagram](https://miro.com/app/board/uXjVOOfgpvA=/?moveToWidget=3458764524926896263&cot=14).

Solution consists of the following parts:

1. **"AuthorizationServer"**.
2. **"AuthorizationServerDb"** contain information about users, user scopes, resource servers, also store the information OIDC needs.
3. **UserManagementPortal**. (To do)

To integrate with AAA service use **"AAA Integration manual.md"** as a instruction.

### **Authorization server purpose usage in enterprise system**
Purpose of the solution is:
1. Provide [OAuth2.0](https://oauth.net/2/?msclkid=7bc0dc74cf8011eca33a0edebedbb800) and [OpenId Connect](https://openid.net/connect/?msclkid=95a6a09ccf8011eca6af98ac2107758b) support.
2. Provide a support of a user management portal (To Do). 

### **Demo applications** 
1. TaskSystemClient -  in OIDC terminology called **'Client'**. This application use **'AuthorizationServer'** to login users(by receiving access, refresh, identity tokens).
2. TaskSystem - in OIDC terminology called **'ResourceServer'**. This application receive requests with **accessToken** and validate it before any endpoint execution.

## Diagrams
<a id="diagrams"></a>

- **Authorization Authentication Accounting** service

 ![image.jpg](images/img192.jpg)

- **Authorization code** request with **PKCE code challenge** [diagram](https://miro.com/app/board/uXjVOOfgpvA=/?moveToWidget=3458764520203912008&cot=14)

 ![image.jpg](images/img195.jpg)


- User authentication 

 ![image.jpg](images/img149.jpg)

## Configuration

<a id="configuration"></a>

### **Authorization server configuration options**

#### Configuration folder: `Certificates`

- **signing-certificate**: contains encrypted with asymmetric encryption signing credentials are used to protect against tampering. Used to encrypt signing credentials on '**AuthorizationServer**'. 
- **encryption-certificate**: contains encryption credentials which are used to ensure the content of '**accessToken**' cannot be read by malicious parties. Used to encrypt jwt '**accessToken**' content on '**Authorization server**'. They can be either asymmetric or symmetric, in this case used asymmetric encryption.
#### Configuration file: `appSettings.json`

- **ClientUrl**: string value. \
Value contains full url of '**Client**' application.
- **PostmanUrl**: string value. \
Value contains postman url of. Use it in testing purposes. 
- **AccessTokenLifetimeInSec**: string value. \
Value contains **access token** lifetime in seconds, should be converted to double before using.
- **RefreshTokenLifetimeInSec**: string value. \
Value contains **refresh token** lifetime in seconds, should be converted to double before using.
- **IdentityTokenLifetimeInSec**: string value. \
Value contains **identity token** lifetime in seconds, should be converted to double before using.
- **AuthorizationCodeLifetimeInSec**: string value. \
Value contains **authorization code** lifetime in seconds, should be converted to double before using.

#### Configuration file: `ConfigurationConstants.cs`

- **LoginPath**: string value. \
Value contains partial pass to a login endpoint.
- **AuthorizationEndpoint**: string value. \
Value contains partial pass to a authorize endpoint.
- **TokenEndpoint**: string value. \
Value contains partial pass to a token endpoint.
- **ClientUrlName**: string value. \
Value contains name of a **'Client'** url value in **'appsettings.json'** file.
- **ConnectionName**: string value. \
Value contains name of a connection string value in **'appsettings.json'** file.

#### Configuration file: `AuthorizationServer\EnvironmentNames.cs`

- **Testing**: string value. \
Value contains testing environment name.

#### Configuration file: `AuthorizationServer\ErrorMessages.cs`

- **LoginFailedErrorMessage**: string value. \
Value contains login failed error message.
- **UserAlreadyExistErrorMessage**: string value. \
Value contains user already exist error message. 
- **OpenIdOperationErrorMessage**: string value. \
Value contains **OIDC** request error message.
- **GrantTypeErrorMessage**: string value. \
Value contains error message with grant type.

#### Configuration file: `Oidc.cs`

- **TestClientIdentifier**: string value. \
Value contains **client identifier** of test **Client**.
- **ClientIdentifier**: string value. \
Value contains **client identifier**. 
- **TestClientDisplayName**: string value. \
Value contains test **'Client'** display name.
- **PostLogoutRedirectUrlQuery**: string value. \
Value contains text of logout redirect url query.

#### Configuration file: `AuthorizationServer.Application\ErrorMessages.cs`

- **ResourceServerWithThisIdNotFoundMessage**: string value. \
Value contains **Resource server** not found error message.

#### Configuration file: `BusinessRules.cs`

- **HasSymbolsPattern**: string value. \
Value contains regex has symbol pattern.
- **HasMinimum8CharsPattern**: string value. \
Value contains regex has minimum 8 characters pattern.
- **HasUpperCharPattern**: string value. \
Value contains regex has upper character pattern.
- **HasLowerCharPattern**: string value. \
Value contains regex has lower character pattern.
- **HasNumberPattern**: string value. \
Value contains regex has number pattern.

#### Configuration file: `AuthorizationServer.Domain\ErrorMessages.cs`

- **UsernameAlreadyExistErrorMessage**: string value. \
Value contains user exist error message.
- **PasswordHasNumberErrorMessage**: string value. \
Value contains password have no numbers error message. 
- **PasswordHasUpperCharErrorMessage**: string value. \
Value contains password has no upper characters error message.
- **PasswordHasMinimum8CharsErrorMessage**: string value. \
Value contains password has less than 8 characters error message.
- **PasswordHasLowerCharErrorMessage**: string value. \
Value contains password has no lower characters error message.
- **PasswordHasSymbolsErrorMessage**: string value. \
Value contains password don't contain special symbols error message.

#### Configuration file: `AuthorizationServer.Infrastructure\EnvironmentNames.cs`

- **Testing**: string value. \
Value contains testing environment name.
- **EnvironmentVariableName**: string value. \
Value contains environment variable name.

#### Configuration file: `TableNames.cs`

- **ResourceServersTable**: string value. \
Value contains **Resource Servers** table name.
- **ScopesTable**: string value. \
Value contains **Scopes** table name.
- **UsersTable**: string value. \
Value contains **Users** table name.
- **UserScopesTable**: string value. \
Value contains **UserScopes** table name.

#### Configuration file: `TablePropertiesLength.cs`

- **UserScopeNameLength**: int value. \
Value contains numeric value of user scope name length.
- **UserUsernameMaxLength**: int value. \
Value contains numeric value of username max length.
- **UserPasswordMaxLength**: int value. \
Value contains numeric value of user password max length.
- **ScopeNameMaxLength**: int value. \
Value contains numeric value of **scope** name max length.
- **ResourceServerNameMaxLength**: int value. \
Value contains numeric value of **resource server** scope same length.

# "AAA Integration manual"

## Table of Contents    
1. [Integration](#integration)
>- 1.1 [Client integration](#client-integration)
>- 1.2 [Resource server integration](#resource-server-integration)
>- 1.3 [Authorization server integration](#authorization-server-integration)
2. [Configuration](#configuration) 
3. [Error handling](#error-handling)

<a id="integration"></a>

## Local setup

<a id="client-integration"></a>

### **Client Setup**

1. Install [react-oauth2-pkce](https://www.npmjs.com/package/react-oauth2-pkce) npm package
>- 1.1 Open terminal in the solution folder
>- 1.2 Run following command: '`npm install --save react-oauth2-pkce`'

2. Wrap App component in context provider '**AuthProvider**' component
>- 2.1 Open '**index.tsx**'
>- 2.2 Import '**AuthProvider**' and '**AuthService**' from '**react-oauth2-pkce**' library. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/index.tsx&version=GBmain&line=7&lineEnd=7&lineStartColumn=1&lineEndColumn=62&lineStyle=plain&_a=contents)
>- 2.3 Create '**authorization service**' object and set its properties from configuration file. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/index.tsx&version=GBmain&line=10&lineEnd=17&lineStartColumn=1&lineEndColumn=4&lineStyle=plain&_a=contents)
>- 2.4 Register '**AuthProvider**' from npm package at the React.DOM and pass '**authorization service**' as parameter. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/index.tsx&version=GBmain&line=21&lineEnd=23&lineStartColumn=1&lineEndColumn=20&lineStyle=plain&_a=contents)

3. Initiate the '**authentication flow**'
>- 3.1 Use '**useAuth**' to lookup the '**authService**'. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/Components/Navbar/Navbar.tsx&version=GBmain&line=9&lineEnd=9&lineStartColumn=3&lineEndColumn=38&lineStyle=plain&_a=contents)
>- 3.2 Use '**authService**' methods to login or logout according to the [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/Components/Navbar/Navbar.tsx&version=GBmain&line=10&lineEnd=11&lineStartColumn=1&lineEndColumn=56&lineStyle=plain&_a=contents)
>- 3.3 To access the '**accessToken**' use '`authService.getAuthTokens().access_token`'. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/Components/Navbar/Navbar.tsx&version=GBmain&line=12&lineEnd=12&lineStartColumn=3&lineEndColumn=58&lineStyle=plain&_a=contents)
>- 3.4 Check if the user is authenticated use '`authService.isAuthenticated()`'. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/Components/Navbar/Navbar.tsx&version=GBmain&line=28&lineEnd=29&lineStartColumn=1&lineEndColumn=1&lineStyle=plain&_a=contents)

4. In order to include '**isAuthorizationEnabled**' header and [OPTIONALLY] include '**AccessToken**' value received from the '**AuthorizationServer**' into the request to the secured '**Resource Server**':
>- 4.1 Install 'https://www.npmjs.com/package/axios npm package'.
>- 4.2 Configure Interceptor to add '**isAuthorizationEnabled**' header and [OPTIONALLY] add '**AccessToken**' value to the Authorization header of the request. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/Interceptors/token.interceptor.tsx&version=GBmain&line=4&lineEnd=17&lineStartColumn=1&lineEndColumn=2&lineStyle=plain&_a=contents)

<a id="resource-server-integration"></a>

### **Resource Server Setup**

1. Install NuGet packages:
>- 1.1 Install [OpenIddict](https://www.nuget.org/packages/OpenIddict/3.1.1?_src=template) to your project.
>- 1.2 Install [OpenIddict.AspNetCore](https://www.nuget.org/packages/OpenIddict.AspNetCore/3.1.1?_src=template) to your project.

2. To validate '**accessToken**' we need to decrypt it using '**Signing**' and '**Encryption**' certificates and verify '**accessToken**' claims.</br> Configure '**OpenIddict**' to achieve it:
>- 2.1 Register '**OpenIddict**' services by calling '`AddOpenIddict()`'. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=36&lineEnd=36&lineStartColumn=1&lineEndColumn=33&lineStyle=plain&_a=contents)
>- 2.2 Register the validation component [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=37&lineEnd=38&lineStartColumn=1&lineEndColumn=6&lineStyle=plain&_a=contents)
>- 2.3 Configure the validation options to verify token 
[Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=39&lineEnd=45&lineStartColumn=8&lineEndColumn=33&lineStyle=plain&_a=contents)
>    - Set '**Issuer**' from configuration file
>    - Use '**Signing**' certificate
>    - Use '**Encryption**' certificate
>    - Setup '**AspNetCore**' as a host for '**OpenIddict**'  
>- 2.4 Add a call to '`AddAuthentication()`' method and set '**OpenIddict**' '**authentication scheme**'. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=22&lineEnd=25&lineStartColumn=1&lineEndColumn=4&lineStyle=plain&_a=contents)
>- 2.5 Add '`UseAuthentication()`' middleware to your pipeline. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=61&lineEnd=61&lineStartColumn=1&lineEndColumn=25&lineStyle=plain&_a=contents)
>- 2.6 Protect '**Resource Server**' Endpoints. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Controllers/TaskController.cs&version=GBmain&line=11&lineEnd=11&lineStartColumn=1&lineEndColumn=125&lineStyle=plain&_a=contents)
>    - Add the '`[Authorize]`' attribute to the controller and pass '**authentication scheme**' as the '**AuthenticationSchemes**' parameter.
3. Add '**Cors**' with '**Client**' origins. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=14&lineEnd=20&lineStartColumn=1&lineEndColumn=9&lineStyle=plain&_a=contents)

4. Authorize users by '**scopes**' from '**accessToken**'
>- 4.1 Install [Microsoft.AspNetCore.Authentication.JwtBearer](https://www.nuget.org/packages/Microsoft.AspNetCore.Authentication.JwtBearer/6.0.3?_src=template) NuGet package to your project.
>- 4.2 Add '`UseAuthorization()`' middleware to your pipeline. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=62&lineEnd=62&lineStartColumn=1&lineEndColumn=24&lineStyle=plain&_a=contents)
>- 4.3 Validate '**scopes**'
>    - Create '**authorization**' requirement class called '**HasScopeRequirement.cs**' [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/ScopeValidation/HasScopeRequirement.cs&version=GBmain&line=1&lineEnd=13&lineStartColumn=1&lineEndColumn=2&lineStyle=plain&_a=contents)
>    - Create '**authorization**' handler class called '**HasScopeHandler.cs**' [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/ScopeValidation/HasScopeHandler.cs&version=GBmain&line=1&lineEnd=18&lineStartColumn=1&lineEndColumn=6&lineStyle=plain&_a=contents)
>    - Register '**HasScopeHandler**' as '**Singleton**' [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=27&lineEnd=27&lineStartColumn=1&lineEndColumn=73&lineStyle=plain&_a=contents)
>- 4.4 Add a call to '`AddAuthorization()`' method. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Program.cs&version=GBmain&line=28&lineEnd=29&lineStartColumn=1&lineEndColumn=32&lineStyle=plain&_a=contents)
>- 4.5 For each '**scope**' call '`AddPolicy()`' method. [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/ScopeValidation/ConfigurePolicy.cs&version=GBmain&line=11&lineEnd=11&lineStartColumn=1&lineEndColumn=111&lineStyle=plain&_a=contents)
>- 4.6 Protect '**Resource Server**' Endpoints [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Controllers/TaskController.cs&version=GBmain&line=11&lineEnd=11&lineStartColumn=1&lineEndColumn=125&lineStyle=plain&_a=contents)
>    - Add the '`[Authorize]`' attribute to the '**Scoped**' action and pass '**scope**' name as the '**Policy**' parameter.

5. Add '**IsAuthorizationEnabled**' configuration and '**EnableAuthorization**' middleware **ToDo: Describe this step**.

6. You need to create Nuget package with '**scopes**' and '**Client**' properties
>- 6.1 Open your solution and Go to: '**SKF.[ApplicationName].[ResourceServerName].AAA**'
>   -  If there is no project with '**SKF.[ApplicationName].[ResourceServerName].AAA**' name, then \
   > Create new Class Library project '**SKF.[ApplicationName].[ResourceServerName].AAA**'

>- 6.2 Create public Enum '**Scopes.cs**' [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-mes?path=/src/modules/laboratory/service/SKF.MES.Laboratory.Module.AAA/Scopes.cs&version=GBdevelopment). 
>   -  Fill up enum with your '**scopes**' values. All scopes names should be named like **'[ControllerName][EndpointName]'**, see the [example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-mes?path=/src/modules/laboratory/service/SKF.MES.Laboratory.Module.AAA/Scopes.cs&version=GBdevelopment&line=6&lineEnd=6&lineStartColumn=9&lineEndColumn=51&lineStyle=plain&_a=contents). 

>- 6.3 Create public Class '**ResourceServerName.cs**' [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-mes?path=/src/modules/laboratory/service/SKF.MES.Laboratory.Module.AAA/ResourceServerName.cs&version=GBdevelopment). 
>  -  Add public const '**Name**' and assign it by '**ResourceServer**' name. 
 
>- 6.4 Create public Class '**ClientProperties.cs**' [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-mes?version=GBdevelopment&path=/src/modules/laboratory/service/SKF.MES.Laboratory.Module.AAA/ClientName.cs)
>   -  Add public const '**ClientIdentifier**' and assign it by value from '**Client**' configuration file. 
>   -  Add public const '**DisplayName**' and assign it
>   -  Add readonly Collection of '**RedirectUris**' and assign it by value from '**Client**' configuration file.

>- 6.5 Commit and Push your changes. 

>- 6.6 Publish Nuget package with your '**scopes**' to the NugetServer. 
>    - 6.6.1 Configure pipeline for publishing Nuget package. You can use this template [link](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-mes?path=/.ci/templates/nuget-build-push.yml). 
>    - 6.6.2 Run pipeline on your branch [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_build?definitionId=648&_a=summary)

<a id="authorization-server-integration"></a>

### **Authorization server Setup**

1. Install your '**SKF.[ApplicationName].[ResourceServerName].AAA**' Nuget package to '**AuthorizationServer**' project
2. Open '**AuthorizationServer.sln**' and Go to src --> AuthorizationServer --> RegisterScopes.cs.
3. Import your '**SKF.[ApplicationName].[ResourceServerName].AAA**' Nuget package [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-aaa?path=/src/AuthorizationServer/RegisterScopes.cs&version=GBmain&line=3&lineEnd=3&lineStartColumn=1&lineEndColumn=37&lineStyle=plain&_a=contents).
4. Duplicate '**mediator.send**' and pass your '**scopes**' and '**resourceServerName**' as method parameters [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-aaa?path=/src/AuthorizationServer/RegisterScopes.cs&version=GBmain&line=18&lineEnd=18&lineStartColumn=13&lineEndColumn=106&lineStyle=plain&_a=contents).
7. Open '**AuthorizationServer.sln**' and Go to src --> AuthorizationServer --> RegisterClients.cs
8. Import your '**SKF.[ApplicationName].[ResourceServerName].AAA**' Nuget package [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-aaa?path=/src/AuthorizationServer/RegisterScopes.cs&version=GBmain&line=3&lineEnd=3&lineStartColumn=1&lineEndColumn=37&lineStyle=plain&_a=contents)
9. Duplicate call to '**RegisterClient**' method and pass your '**ClientIdentifier**' and '**DisplayName**' and '**RedirectUris**' as method parameters [Example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-aaa?path=/src/AuthorizationServer/RegisterScopes.cs&version=GBmain&line=18&lineEnd=18&lineStartColumn=13&lineEndColumn=106&lineStyle=plain&_a=contents)
10. Commit and Push your changes

## Configuration

<a id="configuration"></a>

### **Client configuration options**

#### Configuration file: `.env`

- **REACT_APP_RESOURCE_SERVER_HOST**: string value. \
Value contains full url of '**Resource Server**'.
- **REACT_APP_CLIENT_ID**: string value. \
Value contains identifier of a '**Client**' application, that's use by '**AuthorizationServer**' in order to check if '**Client**' application already registered on '**AuthorizationServer**' or not.
- **REACT_APP_OAUTH_HOST**: string value. \
Value contains full url of '**AuthorizationServer**'.
- **REACT_APP_OAUTH_REDIRECT_URI**: string value. \
Value contains redirect url (url of '**Client**' application). This url will be send on '**AuthorizationServer**' when user start a '**authorization flow**' also this value used by '**AuthorizationServer**' in order to redirect user on a '**Client**' application after he complete a login/registration.
- **REACT_APP_IS_AUTHORIZATION_ENABLED**: boolean value. \
It is recommended to enable this feature in order to turn on '**AuthorizationServer**'.
If '**true**', then to have access to '**Resource Server**' non-public endpoints '**Client**' need to get '**accessToken**' on '**AuthorizationServer**'.
If '**false**' then '**Client**' can send any request without '**accessToken**' validation on ''**Resource Server**'' side.

#### Configuration file: `environment.ts`

- **RESOURCE_SERVER_HOST**: string value. \
Value contains full url of '**Resource Server**', read from file '**.env**'.
- **RESOURCE_SERVER_BASE_PATH**: string value. \
Value contains base path in '**Resource Server**'.
- **CLIENT_ID**: string value. \
Value contains identifier of a '**Client**' application, read from file '**.env**'.
- **OAUTH_HOST**: string value. \
Value contains full url of '**AuthorizationServer**', read from file '**.env**'.
- **OAUTH_REDIRECT_URI**: string value. \
Value contains redirect url (url of '**Client**' application), read from file '**.env**'.
- **isAuthorizationEnabled**: boolean value. \
Contains '**AuthorizationServer**' availability options, read from file '**.env**'.

### **Resource Server configuration options**

#### Configuration folder: `Certificates`

- **signing-certificate**: the same '**signing-certificate**' as on '**AuthorizationServer**', contains encrypted with asymmetric encryption signing credentials are used to protect against tampering. Used to decrypt signing credentials on '**Resource Server**'. Link on a [signing-certificate](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Certificates/signing-certificate.pfx&version=GBmain). 
- **encryption-certificate**: the same '**encryption-certificate**' as on '**AuthorizationServer**', contains encryption credentials which are used to ensure the content of tokens cannot be read by malicious parties. Used to decrypt jwt '**accessToken**' content on '**Resource Server**'. Link on a [encryption-certificate](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.API/Certificates/encryption-certificate.pfx&version=GBmain).

#### Configuration file: `appSettings.json`

- **AuthServerUrl**: string value. \
Value contains full url of '**AuthorizationServer**'.
- **ClientUrl**: string value. \
Value contains full url of '**Client**' application. 
- **isAuthorizationEnabled**: boolean value. \
Value represents enabled '**AuthorizationServer**' or not.
If '**true**', then to access to a non-public controllers '**accessToken**' and '**scopes**' would be validation.
If '**false**' then controllers can be accessed without any validation.


## Errors handling

<a id="error-handling"></a>

### Client identifier errors

#### Error message: The specified 'client_id' is invalid.

to handle this error you need to be sure that 3 following steps were done:

- **Client oidc settings**: check if you have specified **'client_id'** value in oidc settings on a **'Client'** side, this value should match [**client_identifier**](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.Module.AAA/ClientName.cs&version=GBmain&line=5&lineEnd=6&lineStartColumn=1&lineEndColumn=1&lineStyle=plain&_a=contents) value on a **'ResourceServer'**, see the following [example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/task-system-client/src/OAuth/oauth.configuration.tsx&version=GBmain&line=6&lineEnd=6&lineStartColumn=8&lineEndColumn=30&lineStyle=plain&_a=contents). 
- **ResourceServer AAA module(as a nuget package)**: check if you have created class with **'client_identifier'** when you have created '**scopes**' module for **'AuthorizationServer'** on a **'ResourceServer'** side, this identifier should match with identifier that was specified in oidc settings on a **'Client'** side. If it's not, you need to specify it, see the following [example](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-mes-misc?path=/AAAPOC/TaskSystem/TaskSystem.Module.AAA/ClientName.cs&version=GBmain&line=5&lineEnd=6&lineStartColumn=1&lineEndColumn=1&lineStyle=plain&_a=contents). After all corrections you need to rebuild pipeline to publish new version of nuget package and update it in **'AuthorizationServer'**.
- **AuthorizationServer register clients**: check if you have added **'client_identifier'** to [register clients](https://dev.azure.com/skf-digital-manufacturing/SKF-DP-WCM%20Portfolio/_git/2-skf-aaa?path=/src/AuthorizationServer/RegisterClients.cs&version=GBdocumentation/18835-visualization-part&line=30&lineEnd=30&lineStartColumn=38&lineEndColumn=82&lineStyle=plain&_a=contents) function, this variable must be taken from the nuget package(**ResourceServerName.Module.AAA**) that was created on **'ResourceServer'**.
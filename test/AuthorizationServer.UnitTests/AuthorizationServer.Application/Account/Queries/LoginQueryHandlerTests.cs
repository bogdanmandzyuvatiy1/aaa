﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.CQS.Account.Queries.Login;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Application.Account.Queries;

public class LoginQueryHandlerTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly LoginQueryHandler _sut;
    private const string Username = "TestUsername";
    private const string Password = "TestPassword1!";
    
    public LoginQueryHandlerTests()
    {
        var unitOfWorkMock = new Mock<IUnitOfWork>();
        _userRepositoryMock = new Mock<IUserRepository>();
        unitOfWorkMock.Setup(u => u.UserRepository).Returns(_userRepositoryMock.Object);
        _sut = new LoginQueryHandler(unitOfWorkMock.Object);
    }
    
    [Test]
    public async Task Handle_ValidCommand_ReturnClaims()
    {
        // Arrange
        var users = new List<UserEntity>()
        {
            new()
            {
                Username = Username,
                Password = Password
            }
        };
        _userRepositoryMock.Setup(ur => ur.GetAsync(It.IsAny<Expression<Func<UserEntity, bool>>>())).ReturnsAsync(users);
        
        // Act
        var actual = await _sut.Handle(new LoginQuery(Username, Password), default);

        // Assert
        var claims = actual.ShouldBeAssignableTo<ISuccessHandlerResult<LoginQueryResponse>>().Response.Claims;
        Assert.That(claims[0].Value, Is.EqualTo(Username));
        _userRepositoryMock.VerifyAll();
    }
    
    [Test]
    public async Task Handle_ValidCommand_NoUserExists_ReturnNotFound()
    {
        // Arrange
        _userRepositoryMock.Setup(ur => ur.GetAsync(It.IsAny<Expression<Func<UserEntity, bool>>>())).ReturnsAsync(new List<UserEntity>());
        
        // Act
        var actual = await _sut.Handle(new LoginQuery(Username, Password), default);

        // Assert
        actual.ShouldBeAssignableTo<INotFoundHandlerResult<LoginQueryResponse>>();
        _userRepositoryMock.VerifyAll();
    }
}
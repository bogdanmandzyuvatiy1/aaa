﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.CQS.Users.Queries.GetScopesByUserId;
using AuthorizationServer.Application.DTOs.Scopes;
using AuthorizationServer.Infrastructure.Entities;
using AuthorizationServer.Infrastructure.Interfaces;
using AuthorizationServer.Tests.Helpers;
using AutoMapper;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Application.Users.Queries;

public class GetScopesByUserIdQueryHandlerTests
{
    private readonly GetScopesByUserIdQueryHandler _sut;
    private readonly Mock<IMapper> _mapperMock;
    
    public GetScopesByUserIdQueryHandlerTests()
    {
        var unitOfWorkMock = new Mock<IUnitOfWork>();
        unitOfWorkMock.Setup(u => u.ScopeRepository).Returns(new Mock<IScopeRepository>().Object);
        _mapperMock = new Mock<IMapper>();
        _sut = new GetScopesByUserIdQueryHandler(unitOfWorkMock.Object, _mapperMock.Object);
    }

    [Test]
    public async Task Handle_ValidSimpleQuery_ReturnScopeDTOs()
    {
        // Arrange
        var userId = 1;
        var expectedScopes = new List<ScopeDto>
        {
            new()
            {
                Id = 1,
                ScopeName = "UPDATE_ANY_TASK",
            },
            new()
            {
                Id = 2,
                ScopeName = "DELETE_ANY_TASK",
            }
        };
        _mapperMock.Setup(m => m.Map<IEnumerable<ScopeDto>>(It.IsAny<IEnumerable<ScopeEntity>>())).Returns(expectedScopes);

        // Act
        var actual = await _sut.Handle(new GetScopesByUserIdQuery(userId), default);

        // Assert
        var actualScopes = actual.ShouldBeAssignableTo<ISuccessHandlerResult<GetScopesByUserIdQueryResponse>>().Response.ScopeDtos;
        TestHelper.Compare(actualScopes, expectedScopes);
        _mapperMock.VerifyAll();
    }
}
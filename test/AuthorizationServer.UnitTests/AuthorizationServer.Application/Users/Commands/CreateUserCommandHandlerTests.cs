﻿using System.Linq;
using System.Threading.Tasks;
using Art3mis.Common.Core.HandleResults.Abstractions;
using AuthorizationServer.Application.Constants;
using AuthorizationServer.Application.CQS.Users.Commands.CreateUser;
using AuthorizationServer.Tests;
using AuthorizationServer.Tests.Helpers;
using NUnit.Framework;
using Shouldly;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Application.Users.Commands;

public class CreateUserCommandHandlerTests : HandlerTest
{
    
    [Test]
    public async Task CreateUser_WithValidCommand_ReturnsCreated()
    {
        // Arrange
        var userGroup = TestData.CreateUserGroup(1);
        await AuthContext.UserGroups.AddAsync(userGroup); 
        await AuthContext.SaveChangesAsync();
        var expectedUser = TestData.CreateUser(1, userGroupId: userGroup.Id);
        
        // Act
        var result = await Mediator.Send(new CreateUserCommand(expectedUser.Username,
            expectedUser.UserGroupId, expectedUser.Password, expectedUser.FirstName));

        // Assert
        result.ShouldBeAssignableTo<ICreatedHandlerResult<CreateUserCommandResponse>>();
        var actual = AuthContext.Users.First();
        TestHelper.Compare(expectedUser, actual);
    }

    [Test]
    public async Task CreateUser_WhenUsernameExists_ReturnsConflicted()
    {
        // Arrange
        var userGroup = TestData.CreateUserGroup(1);
        await AuthContext.UserGroups.AddAsync(userGroup);
        var expectedUser = TestData.CreateUser(1, userGroupId: userGroup.Id);
        await AuthContext.Users.AddAsync(expectedUser);
        await AuthContext.SaveChangesAsync();
        
        // Act
        var result = await Mediator.Send(new CreateUserCommand(expectedUser.Username,
            expectedUser.UserGroupId, expectedUser.Password, expectedUser.FirstName));

        // Assert
        result.ShouldBeAssignableTo<IConflictHandlerResult<CreateUserCommandResponse>>(); //?.Message
        //.ShouldContain(AuthorizationServer.Domain.Constants.ErrorMessages.UsernameAlreadyExists);
    }
    
    [Test]
    public async Task CreateUser_WhenUserGroupIsNotExisted_ReturnsNotFound()
    {
        // Arrange
        var userGroup = TestData.CreateUserGroup(1);
        var expectedUser = TestData.CreateUser(1, userGroupId: userGroup.Id);
        
        // Act
        var result = await Mediator.Send(new CreateUserCommand(expectedUser.Username,
            expectedUser.UserGroupId, expectedUser.Password, expectedUser.FirstName));

        // Assert
        result.ShouldBeAssignableTo<INotFoundHandlerResult<CreateUserCommandResponse>>()?.Message
            .ShouldContain(ErrorMessages.UserGroupWithSuchIdNotFound);
    }
}
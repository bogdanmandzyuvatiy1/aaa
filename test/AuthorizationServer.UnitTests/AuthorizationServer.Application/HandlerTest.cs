﻿using AuthorizationServer.Extensions;
using AuthorizationServer.Infrastructure.Database;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Application;

public abstract class HandlerTest
{
    protected ServiceProvider Services { get; private set; }
    protected AuthContext AuthContext { get; private set; }
    protected IMediator Mediator { get; private set; }
    
    [SetUp]
    public virtual void Setup()
    {
        var serviceCollection = new ServiceCollection();
        ConfigureServices(serviceCollection);
        Services = serviceCollection.BuildServiceProvider();
        AuthContext = Services.GetRequiredService<AuthContext>();
        Mediator = Services.GetRequiredService<IMediator>();
        AuthContext.Database.EnsureCreated();
    }

    [TearDown]
    public virtual void Dispose()
    {
        AuthContext.Database.EnsureDeleted();
    }
    
    protected virtual void ConfigureServices(ServiceCollection services)
    {
        services.AddCore();
        services.AddDatabase<AuthContext>("", true);
    }
}
﻿using System.Collections.Generic;
using AuthorizationServer.Domain.ResourceServers;
using NUnit.Framework;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Domain.ResourceServers;

public class AddScopesTests
{
    private static readonly List<string> ExpectedScopes =  new List<string>
    {
        new("UPDATE_ANY_TASK"),
        new("DELETE_ANY_TASK"),
        new("ASSIGN_TASK_TO_ANY_USER")
    };
    
    [Test]
    public void AddScopes_GivenScopesAlreadyExist_ShouldRemainTheSameScopesCount()
    {
        // Arrange
        var resourceServer = ResourceServer.Create("TaskSystem", ExpectedScopes).Resource;
        
        //Act
        var actual = resourceServer.AddScopes(ExpectedScopes).Resource;
        
        //Assert
        Assert.That(actual.Scopes.Count, Is.EqualTo(ExpectedScopes.Count));
    }
    
    [Test]
    public void AddScopes_GivenNewScopes_ShouldAddScopes()
    {
        // Arrange
        var resourceServer = ResourceServer.Create("TaskSystem", ExpectedScopes).Resource;
        
        //Act
        var actual = resourceServer.AddScopes(new []{"NEW_SCOPE"}).Resource;
        
        //Assert
        Assert.That(actual.Scopes.Count, Is.GreaterThan(ExpectedScopes.Count));
    }
}
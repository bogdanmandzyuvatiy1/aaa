﻿using System.Collections.Generic;
using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.Roles;
using AuthorizationServer.Domain.Roles.Rules.Interfaces;
using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.Users;
using Moq;
using NUnit.Framework;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Domain.Roles;

public class CreateRoleTests
{
    private readonly Mock<IRoleNameUniquenessChecker> _roleNameUniquenessCheckerMock;
    private const string RoleName = "TestRole";
    private IEnumerable<int> ScopeIds = new List<int>() { 1, 2, 3 };
    private const int UserGroupId = 1;
    private const string Password = "TestPassword1!";

    public CreateRoleTests()
    {
        _roleNameUniquenessCheckerMock = new Mock<IRoleNameUniquenessChecker>();
    }

    [Test]
    public void CreateRole_WhenGivenNameIsNotUnique_BreaksRoleNameMustBeUniqueRule()
    {
        // Arrange
        _roleNameUniquenessCheckerMock.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(false);

        var expectedResult = new DomainModelExecutionResult<Role>
        {
            IsSuccess = false,
            Message = ErrorMessages.RoleAlreadyExists
        };

        // Act
        var actualResult = Role.Create(RoleName, ScopeIds, _roleNameUniquenessCheckerMock.Object);

        // Assert
        Assert.AreEqual(expectedResult.Message, actualResult.Message);
    }

    [Test]
    public void CreateRole_WhenGivenNameIsUnique_IsSuccessful()
    {
        // Arrange
        _roleNameUniquenessCheckerMock.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(true);

        var expectedResult = new DomainModelExecutionResult<User>
        {
            IsSuccess = true
        };

        // Act
        var actualResult = Role.Create(RoleName, ScopeIds, _roleNameUniquenessCheckerMock.Object);

        // Assert
        Assert.AreEqual(expectedResult.IsSuccess, actualResult.IsSuccess);
    }
}
using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.Users;
using AuthorizationServer.Domain.Users.Rules.Interfaces;
using Moq;
using NUnit.Framework;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Domain.Users;

public class RegisterUserTests
{
    private readonly Mock<IUserUniquenessChecker> _userUniquenessCheckerMock;
    private const string Username = "TestUsername";
    private const string Firstname = "Name";
    private const int UserGroupId = 1;
    private const string Password = "TestPassword1!";
    
    public RegisterUserTests()
    {
        _userUniquenessCheckerMock = new Mock<IUserUniquenessChecker>();
    }
    
    [Test]
    public void RegisterUser_WhenGivenUsernameIsNotUnique_BreaksUsernameMustBeUniqueRule()
    {
        // Arrange
        _userUniquenessCheckerMock.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(false);

        var expectedResult = new DomainModelExecutionResult<User>
        {
            IsSuccess = false,
            Message = ErrorMessages.UsernameAlreadyExists
        };
        
        // Act
        var actualResult = User.RegisterUser(Username, UserGroupId, Password, Firstname, _userUniquenessCheckerMock.Object);

        // Assert
        Assert.AreEqual(expectedResult.Message, actualResult.Message);
    }

    [Test]
    public void RegisterUser_WhenUsernameIsUnique_IsSuccessful()
    {
        // Arrange
        _userUniquenessCheckerMock.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(true);

        var expectedResult = new DomainModelExecutionResult<User>
        {
            IsSuccess = true
        };

        // Act
        var actualResult = User.RegisterUser(Username, UserGroupId, Password, Firstname, _userUniquenessCheckerMock.Object);
        
        // Assert
        Assert.AreEqual(expectedResult.IsSuccess, actualResult.IsSuccess);
    }

    [Test]
    public void RegisterUser_WhenGivenPasswordIsValid_IsSuccessful()
    {
        // Arrange
        _userUniquenessCheckerMock.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(true);

        // Act
        var actualResult = User.RegisterUser(Username, UserGroupId, Password, Firstname, _userUniquenessCheckerMock.Object);

        // Assert
        Assert.IsTrue(actualResult.IsSuccess);
    }

    [Test]
    public void RegisterUser_WhenGivenPasswordIsNotValid_BreaksPasswordLenghtCheck()
    {
        // Arrange
        const string password = "Pass";
        _userUniquenessCheckerMock.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(true);

        // Act
        var actualResult = User.RegisterUser(Username, UserGroupId, password, Firstname, _userUniquenessCheckerMock.Object);

        // Assert
        Assert.That(actualResult.Message, Contains.Substring(ErrorMessages.PasswordHasMinimum8Chars));
    }
}
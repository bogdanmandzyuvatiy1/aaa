﻿using AuthorizationServer.Domain.Constants;
using AuthorizationServer.Domain.SeedWork;
using AuthorizationServer.Domain.UserGroups;
using AuthorizationServer.Domain.UserGroups.Rules.Interfaces;
using AuthorizationServer.Domain.Users;
using Moq;
using NUnit.Framework;

namespace AuthorizationServer.UnitTests.AuthorizationServer.Domain.UserGroups;

public class UpdateUserGroupTests
{
    private readonly Mock<IUserGroupUniquenessChecker> _userGroupUniquenessChecker;
    private const string UserGroupName = "TestUserGroupName";
    private readonly UserGroup _userGroup;
    
    public UpdateUserGroupTests()
    {
        _userGroupUniquenessChecker = new Mock<IUserGroupUniquenessChecker>();
        _userGroupUniquenessChecker.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(true);
        _userGroup = UserGroup.Create(UserGroupName, _userGroupUniquenessChecker.Object).Resource;
    }
    
    [Test]
    public void UpdateUserGroup_WhenGivenNameIsNotUnique_BreaksUserGroupNameMustBeUniqueRule()
    {
        // Arrange
        _userGroupUniquenessChecker.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(false);

        var expectedResult = new DomainModelExecutionResult<User>
        {
            IsSuccess = false,
            Message = ErrorMessages.UserGroupNameAlreadyExists
        };
        
        // Act
        var actualResult = _userGroup.Update(UserGroupName, _userGroupUniquenessChecker.Object);

        // Assert
        Assert.AreEqual(expectedResult.Message, actualResult.Message);
    }

    [Test]
    public void UpdateUserGroup_WhenGivenNameIsUnique_IsSuccessful()
    {
        // Arrange
        _userGroupUniquenessChecker.Setup(uc => uc.IsUnique(It.IsAny<string>(), It.IsAny<int>())).Returns(true);

        var expectedResult = new DomainModelExecutionResult<User>
        {
            IsSuccess = true
        };

        // Act
        var actualResult = _userGroup.Update(UserGroupName, _userGroupUniquenessChecker.Object);
        
        // Assert
        Assert.AreEqual(expectedResult.IsSuccess, actualResult.IsSuccess);
    }
}
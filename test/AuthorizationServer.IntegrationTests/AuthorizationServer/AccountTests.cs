﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AuthorizationServer.IntegrationTests.AuthorizationServer.Helpers;
using NUnit.Framework;

namespace AuthorizationServer.IntegrationTests.AuthorizationServer;

public class AccountTests
{
    [Test]
    public async Task EnsureAfterValidUserModelWillReceiveOkStatusCode()
    {
        // Arrange
        var webApplicationFactory = new IntegrationTestWebApplicationFactory<Startup>();
        var client = webApplicationFactory.CreateClient();
        const string username = "dummy";
        const string password = "password";

        var request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:7000/account/login");
        request.Content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("client_id", "task-system-client"),
            new KeyValuePair<string, string>("scope", "openid offline_access"),
            new KeyValuePair<string, string>("response_type", "code"),
            new KeyValuePair<string, string>("redirect_uri", "http://localhost:3000"),
            new KeyValuePair<string, string>("code_challenge", "Ypb6ZW8Kbj6xJGs9OGiToKZe3-WmFHQzacM3qjI0iWg"),
            new KeyValuePair<string, string>("code_challenge_method", "S256"),
            new KeyValuePair<string, string>("Username", username),
            new KeyValuePair<string, string>("Password", password)
        });

        // Act
        var response = await client.SendAsync(request);


        // Assert
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
    }

    [Test]
    public async Task EnsureThatLogoutRequestSendOkResponse()
    {
        // Arrange
        var webApplicationFactory = new IntegrationTestWebApplicationFactory<Startup>();
        using var client = webApplicationFactory.CreateClient();

        var request = new HttpRequestMessage(HttpMethod.Get, "https://localhost:7000/account/logout");
        request.Content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("post_logout_redirect_uri",
                "http://localhost:3000"),
            new KeyValuePair<string, string>("client_id", "task-system-client"),
        });

        // Act
        var response = await client.SendAsync(request);

        // Assert
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
    }
}
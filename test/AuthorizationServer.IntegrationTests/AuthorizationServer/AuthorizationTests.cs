﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AuthorizationServer.IntegrationTests.AuthorizationServer.Helpers;
using NUnit.Framework;

namespace AuthorizationServer.IntegrationTests.AuthorizationServer;

public class AuthorizationTests
{
    [Test]
    public async Task EnsureClientWithValidPkceWillReceiveReceiveOkResponse()
    {
        // Arrange
        var webApplicationFactory = new IntegrationTestWebApplicationFactory<Startup>();
        using var client = webApplicationFactory.CreateClient();

        var request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:7000/authorize");
        request.Content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("client_id", "task-system-client"),
            new KeyValuePair<string, string>("scope", "openid offline_access"),
            new KeyValuePair<string, string>("response_type", "code"),
            new KeyValuePair<string, string>("redirect_uri", "http://localhost:3000"),
            new KeyValuePair<string, string>("code_challenge", "0z68hGaGwa6X4_rN9GbJhA0POsU59VEUiZmC_EHqACc"),
            new KeyValuePair<string, string>("code_challenge_method", "S256"),
        });

        // Act
        var response = await client.SendAsync(request);

        // Assert
        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
    }

    [Test]
    public async Task EnsurePostRequestWithPkceWillReceiveOkResponse()
    {
        // Arrange
        var webApplicationFactory = new IntegrationTestWebApplicationFactory<Startup>();
        using var client = webApplicationFactory.CreateClient();

        var request = new HttpRequestMessage(HttpMethod.Post, "https://localhost:7000/authorize");
        request.Content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("client_id", "task-system-client"),
            new KeyValuePair<string, string>("scope", "openid offline_access"),
            new KeyValuePair<string, string>("response_type", "code"),
            new KeyValuePair<string, string>("redirect_uri", "http://localhost:3000"),
            new KeyValuePair<string, string>("code_challenge", "0z68hGaGwa6X4_rN9GbJhA0POsU59VEUiZmC_EHqACc"),
            new KeyValuePair<string, string>("code_challenge_method", "S256"),
        });

        // Act
        var response = await client.SendAsync(request);

        // Assert

        Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
    }
}
﻿using System;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Tests;

public static class TestData
{
    private static readonly Random Random = new Random();
    private const string Password = "TestPassword1!";
    private const string Username = "TestUsername";
    private const string Name = "Name!";

    public static RoleEntity CreateRole(
        int? id = null,
        string? name = null)
    {
        return new RoleEntity
        {
            Id = id ?? Random.Next(1, 100),
            RoleName = name ?? Name
        };
    }
    
    public static UserEntity CreateUser(
        int? id = null,
        string? name = null,
        string? firstName = null,
        int? userGroupId = null)
    {
        return new UserEntity
        {
            Id = id ?? Random.Next(1, 100) ,
            Username = name ?? Username,
            FirstName = firstName ?? Name,
            Password = Password,
            UserGroupId = userGroupId
        };
    }
    
    public static UserGroupEntity CreateUserGroup(
        int? id = null,
        string? name = null)
    {
        return new UserGroupEntity
        {
            Id = id ?? Random.Next(1, 100),
            UserGroupName = name ?? Name
        };
    }
}
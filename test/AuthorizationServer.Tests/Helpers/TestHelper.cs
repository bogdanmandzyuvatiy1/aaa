﻿using System;
using System.Collections.Generic;
using System.Linq;
using AuthorizationServer.Application.DTOs.Scopes;
using FluentAssertions;
using AuthorizationServer.Infrastructure.Entities;

namespace AuthorizationServer.Tests.Helpers;

public static class TestHelper
{
    public static void Compare(UserEntity actual, UserEntity expected)
    {
        actual.Id.Should().Be(expected.Id);
        actual.Username.Should().Be(expected.Username);
        actual.FirstName.Should().Be(expected.FirstName);
        actual.Password.Should().Be(expected.Password);
        actual.UserGroupId.Should().Be(expected.UserGroupId);
    }
    
    public static void Compare(IEnumerable<ScopeDto> actual, IEnumerable<ScopeDto> expected)
    {
        AssertEquivalent(actual, expected, (e, a) => a.Id == e.Id, Compare);
    }

    public static void Compare(ScopeDto actual, ScopeDto expected)
    {
        actual.Id.Should().Be(expected.Id);
        actual.ScopeName.Should().Be(expected.ScopeName);
        actual.ResourceServerId.Should().Be(expected.ResourceServerId);
    }
    
    public static void AssertEquivalent<T1, T2>(IEnumerable<T1> expected, IEnumerable<T2> actual,
        Func<T1, T2, bool> checkEquivalent, Action<T1, T2> compareAction)
    {
        actual.Count().Should().Be(expected.Count());
        if (expected.Any())
        {
            foreach (var e in expected)
            {
                var a = actual.FirstOrDefault(x => checkEquivalent(e, x));
                a.Should().NotBeNull();
                compareAction(e, a);
            }
        }
    }
}